#pragma once
#include <vector>
#include <glm/glm.hpp>
#include <boost/static_assert.hpp>

#include "ShaderUniformVariable.h"

class ShaderUniformVariableCollection
{
public:

	ShaderUniformVariableCollection() = default;
	ShaderUniformVariableCollection(const std::vector<ShaderUniformVariable<float>>& iFloatUniformVariables, const std::vector<ShaderUniformVariable<glm::mat4>>& iMatrices4UniformVariables, const std::vector<ShaderUniformVariable<glm::vec3>>& iVector3UniformVariables);
	ShaderUniformVariableCollection(const std::vector<ShaderUniformVariableCollection>& iCollectionsToCombine);

	void Add(ShaderUniformVariableCollection iOtherCollection);

	template<typename T>
	void Add(const std::vector<ShaderUniformVariable<T>>& iToAdd)
	{
		auto& ourVariables = GetVariables<T>();
		ourVariables.insert(ourVariables.end(), iToAdd.begin(), iToAdd.end());
	}

	template<typename T>
	void Add(const ShaderUniformVariable<T>& iToAdd)
	{
		auto& ourVariables = GetVariables<T>();
		ourVariables.push_back(iToAdd);
	}

	template<typename T>
	const std::vector<ShaderUniformVariable<T>>& GetVariables() const { return const_cast<ShaderUniformVariableCollection*>(this)->GetVariables<T>(); }

	bool operator==(const ShaderUniformVariableCollection& iOther) const;

private:

	template <typename T>
	std::vector<ShaderUniformVariable<T>>& GetVariables() { BOOST_STATIC_ASSERT_MSG(false, "This uniform variable type is not yet supported. Please add support for it."); }
	template<>
	std::vector<ShaderUniformVariable<float>>& GetVariables() { return mFloatUniformVariables; }
	template<>
	std::vector<ShaderUniformVariable<glm::mat4>>& GetVariables() { return mMatrices4UniformVariables; }
	template<>
	std::vector<ShaderUniformVariable<glm::vec3>>& GetVariables() { return mVector3UniformVariables; }

	std::vector<ShaderUniformVariable<float>> mFloatUniformVariables;
	std::vector<ShaderUniformVariable<glm::mat4>> mMatrices4UniformVariables;
	std::vector<ShaderUniformVariable<glm::vec3>> mVector3UniformVariables;
};

