#pragma once

struct MouseCursorMovement;

class IMouseInputListener
{
public:

	/**
	 * Get the mouse movement that happened in this tick.
	 * The X coordinate increases when moving to the right.
	 * The Y coordinate increases when moving to the top.
	 */
	virtual MouseCursorMovement GetMovementThisTick() = 0;
};

