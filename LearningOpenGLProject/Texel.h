#pragma once

/**
 * Smallest unit of a texture. It is defined by a position in a texture.
 * The position is represented as a UV coordinate. 
 * U is on the horizontal axis.
 * V is on the vertical axis.
 * The (0,0) UV coordinate is located at the bottom left of the texture
 */
struct Texel
{
    Texel() = default;
    Texel(float iU, float iV)
        : U(iU), V(iV)
    {}

    bool operator==(const Texel& iOther) const
    {
        return U == iOther.U && V == iOther.V;
    }

    float U = 0.f;
    float V = 0.f;
};

