#pragma once

#include <gmock/gmock.h>

#include "IOpenGLExtensionsHandler.h"

class MockedOpenGLExtensionHandler : public IOpenGLExtensionsHandler
{
public:
	MOCK_METHOD(void, Initialize, (), (override));
};