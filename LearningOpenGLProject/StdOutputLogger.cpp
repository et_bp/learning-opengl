#include "StdOutputLogger.h"

#include <iostream>

void StdOutputLogger::Write(const std::string& iText)
{
	std::cout << iText << std::endl;
}
