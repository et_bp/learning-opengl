#pragma once

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include "ShaderUniformVariable.h"
#include "Shader.h"

/**
 * Uniform variables that are required to have a movable camera in a shader
 */
class CameraUniformVariables
{
public:

	CameraUniformVariables(
		ShaderUniformVariable<glm::mat4x4> iView,
		ShaderUniformVariable<glm::mat4x4> iModel,
		ShaderUniformVariable<glm::mat4x4> iProjection,
		ShaderUniformVariable<glm::vec3> iCameraPosition);

	/**
	 * Get all uniform variables to add to a shader of the given type
	 * Will return an empty collection if the are no variables for a certain shader type
	 */
	ShaderUniformVariableCollection GetAllVariablesForShader(Shader::Type iForType) const;

	std::string GetViewVariableName() const { return mView.GetName(); }
	std::string GetModelVariableName() const { return mModel.GetName(); }
	std::string GetProjectionVariableName() const { return mProjection.GetName(); }
	std::string GetCameraPositionVariableName() const { return mCameraPosition.GetName(); }

private:

	ShaderUniformVariable<glm::mat4x4> mView;
	ShaderUniformVariable<glm::mat4x4> mModel;
	ShaderUniformVariable<glm::mat4x4> mProjection;
	ShaderUniformVariable<glm::vec3> mCameraPosition;
};

