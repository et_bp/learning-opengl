#pragma once

#include <map>
#include <set>
#include <vector>
#include <GLFW/glfw3.h>
#include <boost/optional.hpp>

#include "IKeyboardInputListener.h"
#include "IMouseInputListener.h"
#include "Keyboard.h"
#include "MouseCursorMovement.h"
#include "MouseCursorPosition.h"
#include "IGlfwWindowEventsListener.h"

class ILogWriter;

/**
 * Logic from the GLFW library to listen to keyboard and moues inputs
 */
class GlfwInputListener : public IKeyboardInputListener, public IMouseInputListener, public IGlfwWindowEventsListener
{

public:

	GlfwInputListener(ILogWriter* iLog);

	// IKeyboardInputListener overrides

	bool IsPressed(Keyboard::Key iWhichKey) const override;
	void When(Keyboard::Action iSomethingHappens, Keyboard::Key iOnAKey, KeyEventCallback iWhatToDo) override;

	// End IKeyboardInputListener overrides

	// IMouseInputListener overrides

	MouseCursorMovement GetMovementThisTick() override;

	// End IMouseInputListener overrides

	// IGlfwWindowEventsListener overrides

	void OnWindowInitialized(GLFWwindow* iWindow) override;
	void BeforePollWindowEvents() override;

	// End IGlfwWindowEventsListener overrides

private:

	/** Informations about a key we're listening to. */
	struct KeyListenedTo
	{
	public:

		KeyListenedTo() = default;
		KeyListenedTo(int iWhichKey, int iWhichAction, KeyEventCallback iCallback)
			: mKeyCodeGlfw(iWhichKey)
		{
			mListeningToActions.push_back(ActionListenedTo(iWhichAction, iCallback));
		}

		/**
		 * Listen to a specific action and define the callback to call when it happens.
		 * If the action is already listened to, replace the callback by the new one
		 */
		void ListenToAction(int iOnAction, KeyEventCallback iWhatToDo);

		/**
		 * If the action is indeed listened to, execute the callback.
		 * If it is not, don't do anything.
		 */
		void NotifyActionHappened(int iWhichAction);

	private:
		struct ActionListenedTo
		{
			ActionListenedTo() = default;
			ActionListenedTo(int iWhichAction, KeyEventCallback iWhatToDo) : KeyActionGlfw(iWhichAction), NotifyItHappened(iWhatToDo) {}
			int KeyActionGlfw;
			IKeyboardInputListener::KeyEventCallback NotifyItHappened;
		};

		int mKeyCodeGlfw;
		std::vector<ActionListenedTo> mListeningToActions;
	};

	static void OnAnyGlfwKeyEvent(GLFWwindow* iInWindow, int iForKey, int iKeyPlatformCode, int iWhichAction, int iCombinedWithModifierKeys);

	void NotifyListeners(int iForKey, int iWhichAction);

	void UpdateWhichKeysArePressed(int iWhichAction, int iForKey);

	static void OnAnyMouseMovement(GLFWwindow* iInWindow, double iAtXPosition, double iAtYPosition);

	// Convert to GLFW types
	static int ConvertToGlfwKey(Keyboard::Key iKey);
	static int ConvertToGlfwAction(Keyboard::Action iAction);

	// Key used in both set and map: the GLFW key int
	std::set<int> mKeysPressed;
	std::map<int, KeyListenedTo> mListeningToKeys;

	// Mouse input listener

	static MouseCursorMovement NoMovement() { return MouseCursorMovement(); };

	/** How much the mouse moved this tick. This is refreshed on every tick. */
	boost::optional<MouseCursorMovement> mMouseMovementThisTick;
	/** Last known position of the mouse in the window. This is not updated if the mouse is out of the window */
	boost::optional<MouseCursorPosition> mMousePosition;

	// Dependencies

	ILogWriter* mLog = nullptr;
	GLFWwindow* mListeningToWindow = nullptr;
};

