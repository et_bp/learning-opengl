#pragma once
#include "IImageImporter.h"

/**
 * Image importer using the Simple OpenGL Image Loader (SOIL), an external header file.
 * You can find all the supported image types as well as the documentation for this plugin here: http://www.lonesock.net/soil.html
 */
class SOILImageImporter : public IImageImporter
{
public:
    std::shared_ptr<Image> ImportFromDisk(std::string iPathOnDisk) override;

private:
    Image::Formats ConvertFromSOILChannels(int iSOILImageChannels);
};

