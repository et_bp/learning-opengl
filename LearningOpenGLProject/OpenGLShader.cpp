#include "OpenGLShader.h"

#include <stdexcept>
#include <cassert>
#include <boost/format.hpp>

#include "OpenGLShaderUtils.h"

OpenGLShader::OpenGLShader(GLenum iType, const std::string& iSourceCode, GLuint iID, const OpenGLUniformVariablesCollection& iUniformVariables)
	: mType(iType)
	, mSourceCode(iSourceCode)
	, mID(iID)
	, mUniformVariables(iUniformVariables)
{

}

std::string OpenGLShader::GetTypeAsString() const
{
	return OpenGLShaderUtils::GLShaderTypeToString(mType);
}

bool OpenGLShader::operator==(const OpenGLShader& iOther) const
{
	return 
		mID == iOther.GetID() && 
		mType == iOther.mType && 
		mSourceCode == iOther.mSourceCode && 
		mUniformVariables == iOther.mUniformVariables;
}
