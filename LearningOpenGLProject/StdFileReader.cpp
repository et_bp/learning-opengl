#include "StdFileReader.h"

#include <fstream>
#include <stdexcept>

std::string StdFileReader::ReadFile(const std::string& iPathRelativeToSolution)
{
	std::string textInFile;
	std::ifstream fileToRead(iPathRelativeToSolution, std::ios::in);

	if (!fileToRead.is_open())
	{
		throw std::runtime_error(std::string("Trying to read '%s', but it couldn't be found.", iPathRelativeToSolution.c_str()));
	}

	std::string lineInFile;
	while (!fileToRead.eof())
	{
		std::getline(fileToRead, lineInFile);
		textInFile.append(lineInFile + "\n");
	}

	fileToRead.close();

	return textInFile;
}
