#pragma once

#include <memory>
#include <functional>

struct Image
{
	enum class Formats
	{
		Unknown, // Used if the format of this image is not yet supported
		RGB,
		RGBA
	};

	Image() = default;
	Image(std::unique_ptr<unsigned char>&& iRawData, int iWidth, int iHeight, Formats iFormat)
		: RawData(std::move(iRawData))
		, Width(iWidth)
		, Height(iHeight)
		, Format(iFormat)
	{}

	Image(Image&& iToMove)
	{
		Format = iToMove.Format;
		Width = iToMove.Width;
		Height = iToMove.Height;
		RawData = std::move(iToMove.RawData);
	}

	bool operator==(const Image& iOther) const
	{
		return
			RawData == iOther.RawData &&
			Format == iOther.Format &&
			Width == iOther.Width &&
			Height == iOther.Height;
	}

	bool IsValid() { return RawData != nullptr; }
	
	Formats Format = Formats::Unknown;

	int Width = 0;
	int Height = 0;

	/**
	 * Data of the image in the form of a array of bytes (char are used since they are of the size of 1 byte)
	 * This raw data is owned by this Image object. If you destroy this image object, the raw data goes with it.
	 * A custom destructor can be set as a lambda function to define how to delete the image since this is dependent on how it was allocated.
	 */
	std::unique_ptr<unsigned char, std::function<void(unsigned char*)>> RawData;
};

