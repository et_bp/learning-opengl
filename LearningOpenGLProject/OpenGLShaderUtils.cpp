#include "OpenGLShaderUtils.h"

#include <boost/assert.hpp>
#include <GL/glew.h>

#include "OpenGLUniformVariablesCollection.h"
#include "ShaderUniformVariableCollection.h"

GLenum OpenGLShaderUtils::ToGLenum(Shader::Type iShaderType)
{
	switch (iShaderType)
	{
	case Shader::Type::Vertex:
		return GL_VERTEX_SHADER;
		break;
	case Shader::Type::Fragment:
		return GL_FRAGMENT_SHADER;
		break;
	default:
		BOOST_ASSERT_MSG(false, "Unsuported conversion from Shader::Type to GLenum shader type");
		return GL_NONE;
		break;
	}
}

std::string OpenGLShaderUtils::GLShaderTypeToString(GLenum iShaderType)
{
	switch (iShaderType)
	{
	case GL_FRAGMENT_SHADER:
		return "Fragment";
		break;
	case GL_VERTEX_SHADER:
		return "Vertex";
		break;
	default:
		BOOST_ASSERT_MSG(false, "Can't convert GLenum shader type to string.");
		return "No to string conversion allowed";
		break;
	}
}

OpenGLUniformVariablesCollection OpenGLShaderUtils::ConvertToOpenGL(const ShaderUniformVariableCollection& iToConvert)
{
	OpenGLUniformVariablesCollection converted;
	converted.Add(ConvertToOpenGL<GLfloat>(iToConvert.GetVariables<float>()));
	converted.Add(ConvertToOpenGL<glm::mat4>(iToConvert.GetVariables<glm::mat4>()));
	converted.Add(ConvertToOpenGL<glm::vec3>(iToConvert.GetVariables<glm::vec3>()));

	return converted;
}