#include "SOILImageImporter.h"

#include <soil.h>
#include <boost/format.hpp>
#include <stdexcept>
#include <memory>

std::shared_ptr<Image> SOILImageImporter::ImportFromDisk(std::string iPathOnDisk)
{
	auto loadedImage = std::make_shared<Image>();

	int loadedImageChannels = 0;

	// Use the feature of a unique_ptr to have a custom destructor to specify how the raw data of the image should be destroyed using SOIL_free_image_data

	auto destructorForImage = [](unsigned char* iToDelete) { SOIL_free_image_data(iToDelete); };
	loadedImage->RawData = 
		std::unique_ptr<unsigned char, std::function<void(unsigned char*)>>(
			SOIL_load_image(iPathOnDisk.c_str(), &loadedImage->Width, &loadedImage->Height, &loadedImageChannels, SOIL_LOAD_AUTO),
			destructorForImage);

	if (!loadedImage->IsValid())
	{
		throw ImageImportError((boost::format("Failed to import image from path on disk: %1%. Error: %2%") % iPathOnDisk % SOIL_last_result()).str());
	}

	loadedImage->Format = ConvertFromSOILChannels(loadedImageChannels);

	if (loadedImage->Format == Image::Formats::Unknown)
	{
		throw ImageImportError((boost::format("Image imported successfully, but the channels (i.e. RGB/RGBA/...) it was imported with are not supported. It was imported with SOIL channels: %1% (see soil.h SOIL_LOAD_*** enum to find the enum matching this value).") % loadedImageChannels).str());
	}

	return loadedImage;
}

Image::Formats SOILImageImporter::ConvertFromSOILChannels(int iSOILImageChannels)
{
	switch (iSOILImageChannels)
	{
	case SOIL_LOAD_RGBA:
		return Image::Formats::RGBA;
		break;
	case SOIL_LOAD_RGB:
		return Image::Formats::RGB;
		break;
	default:
		return Image::Formats::Unknown;
		break;
	}
}
