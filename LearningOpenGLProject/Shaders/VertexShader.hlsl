#version 330                                                                  
																			  
layout (location = 0) in vec3 vertexPosition;											  
layout (location = 1) in vec2 textureCoordinate;
layout (location = 2) in vec3 normal;

out vec2 fragmentShader_textureCoordinate;
out vec3 fragmentShader_normal;
out vec3 fragmentShader_position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()                                                                   
{                               
	vec4 vertexPositionInWorld = model * vec4(vertexPosition, 1.f);

	gl_Position = projection * view * vertexPositionInWorld;

	fragmentShader_position = vertexPositionInWorld.xyz;

	fragmentShader_textureCoordinate = textureCoordinate;

	// mat3 on the model matrix to remove the last column and row. 
	// This is because the translation information shouldn't impact the normal and this information is located in the last row and column.
	// 
	// transpose(inverse(Model)) to fix an issue with the normals when there is a non-uniform scaling (in other words, the scaling is not the same on all axis)
	// More information on this can be read at: https://www.lighthouse3d.com/tutorials/glsl-12-tutorial/the-normal-matrix/
	fragmentShader_normal = mat3(transpose(inverse(model))) * normal;
}