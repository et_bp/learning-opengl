#pragma once
#include "ColorRGB.h"

class Light
{
public:
	Light() = default;
	Light(float iIntensity, const ColorRGB& iColor)
		: mIntensity(iIntensity)
		, mColor(iColor)
	{}

	const ColorRGB& GetColor() const { return mColor; }
	float GetIntensity() const { return mIntensity; }

private:
	float mIntensity = 1.f;
	ColorRGB mColor = {1.f, 1.f, 1.f};
};

