#pragma once

#include "IOpenGLLibrary.h"
#include "Image.h"

class Mesh;

/**
 * Wrapper around the OpenGL API. All features provided by the API should be added here.
 */
class OpenGLWrapper : public IOpenGLLibrary
{
public:
	// IOpenGLLibrary overrides begin
	void EnableCapability(GLenum iCapability) override;

	void SetViewportDimensions(const Dimensions2D& iDimensions) override;
	void ClearViewport(const ColorRGBA& iWithColor) override;


	OpenGLTexture CreateTexture(const Image* const iFromImage) override;

	OpenGLMesh CreateMesh(const Mesh& iFromMesh) override;

	void DrawMesh(const OpenGLMesh& iToDraw) override;

	OpenGLShader CreateShader(const Shader& iFromShader) const override;
	void CompileShader(const OpenGLShader& iToCompile) const override;

	OpenGLShaderProgram CreateShaderProgram() override;
	void UseShaderProgram(const OpenGLShaderProgram& iToUse) override;
	void ClearShaderProgram() override;
	void LinkShaderProgram(const OpenGLShaderProgram& iToLink) override;
	void ValidateShaderProgram(const OpenGLShaderProgram& iToValidate) override;
	void AttachShaderToProgram(const OpenGLShader& iShader, OpenGLShaderProgram& ioAttachTo) override;

	void BindUniformVariablesInProgram(OpenGLShaderProgram& ioProgram) override;

	void SetUniformVariable(const OpenGLUniformVariableTyped<GLfloat>& iFloat) override;
	void SetUniformVariable(const OpenGLUniformVariableTyped<glm::mat4>& iMatrix4) override;
	void SetUniformVariable(const OpenGLUniformVariableTyped<glm::vec3>& iVector3) override;

	// IOpenGLLibrary overrides end

private:
	
	std::vector<GLfloat> CombineAllVerticesData(const std::vector<glm::vec3>& iVertices, const std::vector<Texel>& iTextureMapping, const std::vector<glm::vec3>& iNormals);

	GLenum ConvertToOpenGL(Image::Formats iImageFormat) const;

	void BindTextureToSampler(GLenum iShaderTextureSampler, const OpenGLTexture& iTexture);

	void ValidateProgramIsLinked(GLuint iProgramID);
	void ValidateProgramCanExecute(GLuint idOfProgramToValidate);

	GLuint ClearVertexArray() { return 0; }
};

