#include "PhongWorldRenderer.h"

#include <boost/format.hpp>

#include "IRenderer3D.h"
#include "ColorRGBA.h"
#include "Shader.h"
#include "AmbientLight.h"
#include "DirectionalLight.h"

PhongWorldRenderer::PhongWorldRenderer(IRenderer3D* iRenderer)
	: mRenderer(iRenderer)
{

}

void PhongWorldRenderer::SetShaders(const std::string& iVertexShaderCode, const std::string& iFragmentShaderCode, const PhongLightingUniformVariables& iPhongUniformVariables, const CameraUniformVariables& iCameraUniformVariables)
{

	mRenderer->SetShader(
		{ 
			Shader::Type::Vertex, 
			iVertexShaderCode, 
			{{iPhongUniformVariables.GetAllVariablesForShader(Shader::Type::Vertex), iCameraUniformVariables.GetAllVariablesForShader(Shader::Type::Vertex)}} 
		});
	mRenderer->SetShader(
		{
			Shader::Type::Fragment, 
			iFragmentShaderCode, 
			{{iPhongUniformVariables.GetAllVariablesForShader(Shader::Type::Fragment), iCameraUniformVariables.GetAllVariablesForShader(Shader::Type::Fragment)}}
		});

	mRenderer->AllShadersAreSet();

	mPhongUniformVariables = iPhongUniformVariables;
	mCameraUniformVariables = iCameraUniformVariables;
}

void PhongWorldRenderer::SetAmbientLight(const AmbientLight& iNewAmbientLight)
{
	if (!mPhongUniformVariables.has_value())
	{
		throw NoShaderSetError("Failed to set the ambient light since no shaders are set. Make sure to call SetShaders first.");
	}

	mRenderer->SetValueInUniformVariable(mPhongUniformVariables->GetAmbientLightIntensityVariableName(), iNewAmbientLight.GetIntensity());
	mRenderer->SetValueInUniformVariable(mPhongUniformVariables->GetAmbientLightColorVariableName(), glm::vec3(iNewAmbientLight.GetColor()));
}

void PhongWorldRenderer::SetDirectionalLight(const DirectionalLight& iNewDirectionalLight)
{
	if (!mPhongUniformVariables.has_value())
	{
		throw NoShaderSetError("Failed to set the directional light since no shaders are set. Make sure to call SetShaders first.");
	}

	mRenderer->SetValueInUniformVariable(mPhongUniformVariables->GetDirectionalLightIntensityVariableName(), iNewDirectionalLight.GetIntensity());
	mRenderer->SetValueInUniformVariable(mPhongUniformVariables->GetDirectionalLightColorVariableName(), glm::vec3(iNewDirectionalLight.GetColor()));
	mRenderer->SetValueInUniformVariable(mPhongUniformVariables->GetDirectionalLightDirectionVariableName(), iNewDirectionalLight.GetDirection());
}

void PhongWorldRenderer::SetProjectionMatrix(const glm::mat4x4& iNewProjectionMatrix)
{
	if (!mCameraUniformVariables.has_value())
	{
		throw NoShaderSetError("Failed to set the projection matrix since no shaders are set. Make sure to call SetShaders before updating the projection.");
	}

	mRenderer->SetValueInUniformVariable(mCameraUniformVariables->GetProjectionVariableName(), iNewProjectionMatrix);
}

void PhongWorldRenderer::SetModelmatrix(const glm::mat4x4& iNewModelMatrix)
{
	if (!mCameraUniformVariables.has_value())
	{
		throw NoShaderSetError("Failed to set the model matrix since no shaders are set. Make sure to call SetShaders first.");
	}

	mRenderer->SetValueInUniformVariable(mCameraUniformVariables->GetModelVariableName(), iNewModelMatrix);
}

void PhongWorldRenderer::AddMeshToWorld(std::unique_ptr<Mesh>&& iMeshToRender)
{
	try
	{
		mMeshesToRender.push_back({ std::move(iMeshToRender), mRenderer->CreateMesh(*iMeshToRender.get()) });
	}
	catch (const std::invalid_argument& iError)
	{
		throw InvalidMeshError((boost::format("Failed to the mesh to world because it couldn't be created in the 3D Renderer. Error: %1%") % iError.what()).str());
	}
}

void PhongWorldRenderer::AddMeshesToWorld(std::vector<std::unique_ptr<Mesh>>&& iMeshesToAdd)
{
	std::vector<unsigned int> meshIndexesAddedToWorld;

	auto meshToAddIterator = iMeshesToAdd.begin();
	while (meshToAddIterator != iMeshesToAdd.end())
	{
		try
		{
			AddMeshToWorld(std::move(*meshToAddIterator));
			// The mesh created successfully, remove it from the list passed in parameters
			// The iterator returned by erase is the next element in the list
			meshToAddIterator = iMeshesToAdd.erase(meshToAddIterator);
		}
		catch (const InvalidMeshError& /*iError*/)
		{
			// Since the mesh is invalid, it is not added to the world.
			// Go the next mesh
			++meshToAddIterator;
		}
	}
}

void PhongWorldRenderer::UpdatePlayerCamera(const glm::vec3& iPosition, const glm::mat4x4& iViewMatrix)
{
	if (!mCameraUniformVariables.has_value())
	{
		throw NoShaderSetError("Failed to update the player camera since no shaders are set. Make sure to call SetShaders before updating the player camera.");
	}

	mRenderer->SetValueInUniformVariable(mCameraUniformVariables->GetCameraPositionVariableName(), iPosition);
	mRenderer->SetValueInUniformVariable(mCameraUniformVariables->GetViewVariableName(), iViewMatrix);
}

void PhongWorldRenderer::Render()
{
	if (!mPhongUniformVariables.has_value())
	{
		throw NoShaderSetError("Failed to render since no shaders are set. Make sure to call SetShaders first.");
	}

	mRenderer->SetBackgroundColor(ColorRGBA::Black);

	for (const MeshCreatedInRenderer& meshToRender : mMeshesToRender)
	{
		const Material& materialOnMesh = meshToRender.MeshCreated->GetMaterial();
		if (materialOnMesh.IsValid())
		{
			SetMaterialUniformVariables(materialOnMesh);

		}

		mRenderer->DrawMesh(meshToRender.IdInRenderer);
	}
	
}

void PhongWorldRenderer::SetMaterialUniformVariables(const Material& iMaterial)
{
	mRenderer->SetValueInUniformVariable(
		mPhongUniformVariables->GetMaterialSpecularIntensityVariableName(),
		iMaterial.GetSpecularIntensity());

	mRenderer->SetValueInUniformVariable(
		mPhongUniformVariables->GetMaterialSpecularPrecisionVariableName(),
		iMaterial.GetSpecularPrecision());
}

PhongWorldRenderer::MeshCreatedInRenderer::MeshCreatedInRenderer(std::unique_ptr<Mesh>&& iMeshCreated, const MeshUniqueIdInRenderer& iIdInRenderer)
	: MeshCreated(std::move(iMeshCreated))
	, IdInRenderer(iIdInRenderer)
{

}
