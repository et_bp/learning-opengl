#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <memory>

#include <glm/vec3.hpp>

#include <FlyingPlayerCameraController.h>
#include <Keyboard.h>
#include <PlayerCamera.h>
#include <AircraftRotation.h>

#include "MockedKeyboardInputListener.h"
#include "MockedMouseInputListener.h"

using ::testing::Return;
using ::testing::Exactly;
using ::testing::AtLeast;
using ::testing::_;

class FlyingPlayerCameraControllerTest : public ::testing::Test
{
protected:
	void SetUp() override
	{
		ControlledCamera = std::make_shared<PlayerCamera>(InitialCameraPosition, AircraftRotation(0.f, 0.f, 0.f));

		KeyboardInputListener = std::make_shared<MockedKeyboardInputListener>();
		MouseInputListener = std::make_shared<MockedMouseInputListener>();

		UnderTest = std::make_unique<FlyingPlayerCameraController>(ControlledCamera, KeyboardInputListener, MouseInputListener, CameraMovementSpeed, CameraMovementControls);

		// No key presses by default
		EXPECT_CALL(*KeyboardInputListener, IsPressed(_))
			.WillRepeatedly(Return(false));

		// No mouse movement by default
		EXPECT_CALL(*MouseInputListener, GetMovementThisTick())
			.WillRepeatedly(Return(MouseCursorMovement()));
	}

	std::unique_ptr<FlyingPlayerCameraController> UnderTest;


	Keyboard::Key ForwardKey = Keyboard::Key::W;
	Keyboard::Key BackwardKey = Keyboard::Key::S;
	Keyboard::Key StrafeRightKey = Keyboard::Key::D;
	Keyboard::Key StrafeLeftKey = Keyboard::Key::A;

	FlyingPlayerCameraController::SpeedParameters CameraMovementSpeed = { 1.f, 1.f };
	FlyingPlayerCameraController::Controls CameraMovementControls = { ForwardKey, BackwardKey, StrafeRightKey, StrafeLeftKey };

	glm::vec3 InitialCameraPosition = { 0.f, 0.f, 0.f };
	glm::vec3 ForwardInWorld = {1.f, 0.f, 0.f};
	// Dependencies
	std::shared_ptr<PlayerCamera> ControlledCamera;
	std::shared_ptr<MockedKeyboardInputListener> KeyboardInputListener;
	std::shared_ptr<MockedMouseInputListener> MouseInputListener;
};

TEST_F(FlyingPlayerCameraControllerTest, PressingForwardMovesCameraForward)
{
	EXPECT_CALL(*KeyboardInputListener, IsPressed(ForwardKey))
		.Times(AtLeast(1))
		.WillRepeatedly(Return(true));

	float timePassed = 1.f;
	UnderTest->Tick(timePassed);
	UnderTest->MoveCamera();

	EXPECT_EQ(InitialCameraPosition + timePassed * CameraMovementSpeed.MovementSpeed * ForwardInWorld, ControlledCamera->GetPosition());
}
