#pragma once

namespace Keyboard
{
	enum class Key
	{
		ESC,
		W,
		A,
		S,
		D
	};

	enum class Action
	{
		Press
	};
}

