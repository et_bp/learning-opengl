#pragma once

#include <glm/vec3.hpp>

struct ColorRGB
{
	ColorRGB(float iRed, float iBlue, float iGreen)
		: Red(iRed)
		, Blue(iBlue)
		, Green(iGreen)
	{}

	operator glm::vec3() const
	{
		return glm::vec3(Red, Blue, Green);
	}

	float Red = 1.f;
	float Blue = 1.f;
	float Green = 1.f;
};

