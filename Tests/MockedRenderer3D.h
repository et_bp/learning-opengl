#pragma once

#include "IRenderer3D.h"

#include <gmock/gmock.h>

#include <string>
#include <glm/glm.hpp>

#include "Dimensions2D.h"
#include "Mesh.h"
#include "Shader.h"
#include "MeshUniqueIdInRenderer.h"

class MockedRenderer3D : public IRenderer3D
{
public:
	MOCK_METHOD(void, Initialise, (const Dimensions2D& iViewportDimensions), (override));
	MOCK_METHOD(void, SetBackgroundColor, (const ColorRGBA& iColor), (override));
	MOCK_METHOD(void, DrawMesh, (const MeshUniqueIdInRenderer& iMeshId), (override));
	MOCK_METHOD(MeshUniqueIdInRenderer, CreateMesh, (const Mesh& iToAdd), (override));
	MOCK_METHOD(bool, IsMeshCreated, (const MeshUniqueIdInRenderer& iMeshId), (const, override));
	MOCK_METHOD(void, SetShader, (const Shader& iShader), (override));
	MOCK_METHOD(void, AllShadersAreSet, (), (override));
	MOCK_METHOD(void, SetValueInUniformVariable, (const std::string& iVariableName, float iNewValue), (override));
	MOCK_METHOD(void, SetValueInUniformVariable, (const std::string& iVariableName, const glm::vec3& iNewValue), (override));
	MOCK_METHOD(void, SetValueInUniformVariable, (const std::string& iVariableName, const glm::mat4& iNewValue), (override));
};

