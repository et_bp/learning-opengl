#include "PlayerCamera.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "AircraftRotation.h"
#include "CameraMovement.h"

PlayerCamera::PlayerCamera(const glm::vec3& iInitialPosition, const AircraftRotation& iInitialRotation)
	: mPosition(iInitialPosition)
	, mYaw(iInitialRotation.Yaw)
	, mPitch(iInitialRotation.Pitch)
{
	UpdateOrientationFromRotation();
}

void PlayerCamera::Move(const AircraftRotation& iRotation, const CameraMovement& iMovement)
{
	mYaw += iRotation.Yaw;
	mPitch = glm::clamp(mPitch + iRotation.Pitch, -89.f, 89.f);

	UpdateOrientationFromRotation();

	MovePosition(iMovement);
}

void PlayerCamera::MovePosition(const CameraMovement& iMovement)
{
	if (glm::abs(iMovement.Forward) > 0.f)
	{
		mPosition += mFront * iMovement.Forward;
	}

	if (glm::abs(iMovement.Right) > 0.f)
	{
		mPosition += mRight * iMovement.Right;
	}
}

glm::mat4 PlayerCamera::ComputeViewMatrix()
{
	return glm::lookAt(mPosition, mPosition + mFront, mUp);
}

void PlayerCamera::UpdateOrientationFromRotation()
{
	const static glm::vec3 worldUp = { 0.f, 1.f, 0.f };

	float yawInRadians = glm::radians(mYaw);
	float pitchInRadians = glm::radians(mPitch);

	mFront.x = cos(yawInRadians) * cos(pitchInRadians);
	mFront.y = sin(pitchInRadians);
	mFront.z = sin(yawInRadians) * cos(pitchInRadians);
	mFront = glm::normalize(mFront);

	mRight = glm::normalize(glm::cross(mFront, worldUp));

	mUp = glm::normalize(glm::cross(mRight, mFront));
}


