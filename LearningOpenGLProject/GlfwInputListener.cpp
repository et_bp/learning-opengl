#include "GlfwInputListener.h"

#include <map>
#include <stdexcept>
#include <boost/assert.hpp>
#include <glm/glm.hpp>

#include "ILogWriter.h"

GlfwInputListener::GlfwInputListener(ILogWriter* iLog)
	: mLog(iLog)
{
}

bool GlfwInputListener::IsPressed(Keyboard::Key iWhichKey) const
{
	return mKeysPressed.find(ConvertToGlfwKey(iWhichKey)) != mKeysPressed.end();
}

void GlfwInputListener::When(Keyboard::Action iSomethingHappens, Keyboard::Key iOnAKey, KeyEventCallback iWhatToDo)
{
	auto keyboardKeyGlfw = ConvertToGlfwKey(iOnAKey);
	auto keyboareActionGlfw = ConvertToGlfwAction(iSomethingHappens);

	auto iteratorToKeyFound = mListeningToKeys.find(ConvertToGlfwKey(iOnAKey));
	if (iteratorToKeyFound != mListeningToKeys.end())
	{
		iteratorToKeyFound->second.ListenToAction(ConvertToGlfwAction(iSomethingHappens), iWhatToDo);
	}
	else
	{
		mListeningToKeys[keyboardKeyGlfw] = KeyListenedTo(keyboardKeyGlfw, keyboareActionGlfw, iWhatToDo);
	}
}

void GlfwInputListener::OnAnyGlfwKeyEvent(GLFWwindow* iInWindow, int iForKey, int iKeyPlatformCode, int iWhichAction, int iCombinedWithModifierKeys)
{
	GlfwInputListener* thisPtr = static_cast<GlfwInputListener*>(glfwGetWindowUserPointer(iInWindow));
	BOOST_ASSERT(thisPtr);

	thisPtr->UpdateWhichKeysArePressed(iWhichAction, iForKey);
	thisPtr->NotifyListeners(iForKey, iWhichAction);
}

void GlfwInputListener::NotifyListeners(int iForKey, int iWhichAction)
{
	auto iteratorToKeyFound = mListeningToKeys.find(iForKey);
	if (iteratorToKeyFound != mListeningToKeys.end())
	{
		iteratorToKeyFound->second.NotifyActionHappened(iWhichAction);
	}
}

void GlfwInputListener::UpdateWhichKeysArePressed(int iWhichAction, int iForKey)
{
	switch (iWhichAction)
	{
	case GLFW_PRESS:
		BOOST_ASSERT(mKeysPressed.find(iForKey) == mKeysPressed.end());
		mKeysPressed.insert(iForKey);
		break;
	case GLFW_RELEASE:
		BOOST_ASSERT(mKeysPressed.find(iForKey) != mKeysPressed.end());
		mKeysPressed.erase(iForKey);
		break;
	case GLFW_REPEAT:
		// Nothing to do when repeating since the key has already been pressed
		break;
	default:
		BOOST_ASSERT_MSG(false, "Unsupported GLFW key action.");
		break;
	}
}

int GlfwInputListener::ConvertToGlfwKey(Keyboard::Key iKey)
{
	switch (iKey)
	{
	case Keyboard::Key::ESC:
		return GLFW_KEY_ESCAPE;
		break;
	case Keyboard::Key::W:
		return GLFW_KEY_W;
	case Keyboard::Key::A:
		return GLFW_KEY_A;
	case Keyboard::Key::S:
		return GLFW_KEY_S;
	case Keyboard::Key::D:
		return GLFW_KEY_D;
	default:
		BOOST_ASSERT_MSG(false, "Unhandled Keyboard::Key conversion to GLFW key.");
		return GLFW_KEY_ESCAPE;
		break;
	}
}

int GlfwInputListener::ConvertToGlfwAction(Keyboard::Action iAction)
{
	switch (iAction)
	{
	case Keyboard::Action::Press:
		return GLFW_PRESS;
		break;
	default:
		BOOST_ASSERT_MSG(false, "Unhandled Keyboard::Action conversion to GLFW action.");
		return GLFW_PRESS;
		break;
	}
}

/************************************************************************/
/* IGlfwWindowEventsListener                                            */
/************************************************************************/

void GlfwInputListener::OnWindowInitialized(GLFWwindow* iWindow)
{
	mLog->Write("Listening to Keyboard and Mouse inputs from window with GLFW.");
	mListeningToWindow = iWindow;

	glfwSetWindowUserPointer(mListeningToWindow, this);
	glfwSetKeyCallback(mListeningToWindow, OnAnyGlfwKeyEvent);
	glfwSetCursorPosCallback(mListeningToWindow, OnAnyMouseMovement);

	mLog->Write("Hiding Mouse cursor.");
	glfwSetInputMode(iWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void GlfwInputListener::BeforePollWindowEvents()
{
	mMouseMovementThisTick.reset();
}

/************************************************************************/
/* Mouse Input Listener                                                 */
/************************************************************************/

MouseCursorMovement GlfwInputListener::GetMovementThisTick()
{
	return mMouseMovementThisTick.get_value_or(NoMovement());
}

void GlfwInputListener::OnAnyMouseMovement(GLFWwindow* iInWindow, double iAtXPosition, double iAtYPosition)
{
	GlfwInputListener* thisPtr = static_cast<GlfwInputListener*>(glfwGetWindowUserPointer(iInWindow));

	double bottomToTopY = -1 * iAtYPosition;

	MouseCursorPosition newMousePosition({ iAtXPosition, bottomToTopY });
	if (thisPtr->mMousePosition.has_value())
	{
		// Only works if this event is called once by GLFW. If it is called multiple times, this logic will only keep the last movement
		thisPtr->mMouseMovementThisTick = MouseCursorMovement(thisPtr->mMousePosition.get(), newMousePosition);
	}

	thisPtr->mMousePosition = newMousePosition;
}

/************************************************************************/
/* KeyListenedTo class                                                  */
/************************************************************************/

void GlfwInputListener::KeyListenedTo::ListenToAction(int iOnAction, KeyEventCallback iWhatToDo)
{
	for (ActionListenedTo& actionListenedTo : mListeningToActions)
	{
		if (actionListenedTo.KeyActionGlfw == iOnAction)
		{
			actionListenedTo.NotifyItHappened = iWhatToDo;
			return;
		}
	}

	mListeningToActions.push_back(ActionListenedTo(iOnAction, iWhatToDo));
}

void GlfwInputListener::KeyListenedTo::NotifyActionHappened(int iWhichAction)
{
	for (const ActionListenedTo& actionListenedTo : mListeningToActions)
	{
		if (actionListenedTo.KeyActionGlfw == iWhichAction)
		{
			actionListenedTo.NotifyItHappened();
		}
	}
}
