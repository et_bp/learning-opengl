#pragma once
#include <string>
#include <memory>
#include <GL/glew.h>

#include "Shader.h"
#include "OpenGLUniformVariablesCollection.h"

struct OpenGLShader
{
public:

	OpenGLShader() = default;
	OpenGLShader(GLenum iType, const std::string& iSourceCode, GLuint iID, const OpenGLUniformVariablesCollection& iUniformVariables);

	GLuint GetID() const { return mID; }
	void SetID(GLuint iID) { mID = iID; }

	GLenum GetType() const { return mType; }
	std::string GetTypeAsString() const;
	void SetType(GLenum iType) { mType = iType; }

	std::string GetSourceCode() const { return mSourceCode; }
	void SetSourceCode(std::string iSourceCode) { mSourceCode = iSourceCode; }

	OpenGLUniformVariablesCollection GetUniformVariables() const { return mUniformVariables; }
	void SetUniformVariables(const OpenGLUniformVariablesCollection& val) { mUniformVariables = val; }

	bool operator==(const OpenGLShader& iOther) const;

	friend std::ostream& operator<<(std::ostream& iOutStream, const OpenGLShader& iShader)
	{
		iOutStream << iShader.GetTypeAsString() << " Shader (ID=" << iShader.GetID() << ")";
		return iOutStream;
	}

private:

	GLenum mType;
	GLuint mID = 0;
	std::string mSourceCode;
	OpenGLUniformVariablesCollection mUniformVariables;
};
