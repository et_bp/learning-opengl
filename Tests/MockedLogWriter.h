#pragma once

#include <gmock/gmock.h>

#include "ILogWriter.h"

class MockedLogWriter : public ILogWriter
{
public:
	MOCK_METHOD(void, Write, (const std::string& iText), (override));
};

