#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>
#include <vector>
#include <unordered_set>

#include "OpenGLRenderer.h"
#include "MockedLogWriter.h"
#include "MockedOpenGLExtensionHandler.h"
#include "MockedOpenGLLibrary.h"
#include "Dimensions2D.h"
#include "Mesh.h"
#include "ShaderBuilder.h"
#include "OpenGLShader.h"
#include "OpenGLShaderUtils.h"
#include "Image.h"
#include "OpenGLTexture.h"
#include "TestObjects.h"
#include "MeshUniqueIdInRenderer.h"

using ::testing::AnyNumber;
using ::testing::_;
using ::testing::Return;
using ::testing::InSequence;

/**
 * Test fixture where the OpenGLRenderer is not initialized
 */
class OpenGLRendererUninitializedTest : public ::testing::Test
{
protected:
	void SetUp() override
	{
		MockedOpenGL = std::make_unique<MockedOpenGLLibrary>();
		MockedExtensionsHandler = std::make_unique<MockedOpenGLExtensionHandler>();
		MockedLog = std::make_unique<MockedLogWriter>();

		OpenGLRendererToTest = std::make_unique<OpenGLRenderer>(MockedOpenGL.get(), MockedExtensionsHandler.get(), MockedLog.get());

		EXPECT_CALL(*MockedLog, Write(_))
			.Times(AnyNumber());
	}

	std::unique_ptr<OpenGLRenderer> OpenGLRendererToTest;

	// Dependencies
	std::unique_ptr<MockedLogWriter> MockedLog;
	std::unique_ptr<MockedOpenGLExtensionHandler> MockedExtensionsHandler;
	std::unique_ptr<MockedOpenGLLibrary> MockedOpenGL;
};

TEST_F(OpenGLRendererUninitializedTest, OpenGLIsInitializedProperly)
{
	OpenGLShaderProgram createdShaderProgram(1);
	Dimensions2D viewportDimensions(800, 600);

	{
		InSequence orderedExpectations;

		EXPECT_CALL(*MockedExtensionsHandler, Initialize()).Times(1);
		EXPECT_CALL(*MockedOpenGL, SetViewportDimensions(viewportDimensions)).Times(1);
		EXPECT_CALL(*MockedOpenGL, CreateShaderProgram()).Times(1).WillOnce(Return(createdShaderProgram));
	}

	OpenGLRendererToTest->Initialise({ 800, 600 });

	EXPECT_EQ(createdShaderProgram.GetID(), OpenGLRendererToTest->GetCurrentShaderProgram()->GetID()) << "Shader program has not been set as current shader program.";
}

/**
 * Test fixture where the OpenGLRenderer is already initialized
 */
class OpenGLRendererTest : public ::testing::Test
{
protected:

	void SetUp() override
	{
		MockedOpenGL = std::make_unique<MockedOpenGLLibrary>();
		MockedExtensionsHandler = std::make_unique<MockedOpenGLExtensionHandler>();
		MockedLog = std::make_unique<MockedLogWriter>();

		OpenGLRendererToTest = std::make_unique<OpenGLRenderer>(MockedOpenGL.get(), MockedExtensionsHandler.get(), MockedLog.get());

		EXPECT_CALL(*MockedLog, Write(_))
			.Times(AnyNumber());

		Initialize();
	}

	void Initialize()
	{
		Dimensions2D viewportDimensions(800, 600);

		EXPECT_CALL(*MockedExtensionsHandler, Initialize()).Times(1);
		EXPECT_CALL(*MockedOpenGL, SetViewportDimensions(viewportDimensions)).Times(1);
		EXPECT_CALL(*MockedOpenGL, CreateShaderProgram()).Times(1).WillOnce(Return(mShaderProgramInRenderer));
		OpenGLRendererToTest->Initialise(viewportDimensions);
	}


	OpenGLMesh CreateOpenGLMeshForTest(const Mesh& iFrom)
	{
		GLuint vao = 1000, vbo = 1000, ibo = 1000;
		return OpenGLMesh(
			iFrom.GetIndices(),
			iFrom.GetVertices(),
			iFrom.GetNormals(), vao, vbo, ibo);
	}

	OpenGLShaderProgram mShaderProgramInRenderer = OpenGLShaderProgram(1);

	std::unique_ptr<OpenGLRenderer> OpenGLRendererToTest;


	// Dependencies
	std::unique_ptr<MockedLogWriter> MockedLog;
	std::unique_ptr<MockedOpenGLExtensionHandler> MockedExtensionsHandler;
	std::unique_ptr<MockedOpenGLLibrary> MockedOpenGL;
};

TEST_F(OpenGLRendererTest, MeshIsCreatedInOpenGL)
{
	Mesh toCreateInRendererPointer = TestObjects::CreateTriangleMesh();

	EXPECT_CALL(*MockedOpenGL, CreateMesh(toCreateInRendererPointer))
		.WillOnce(Return(CreateOpenGLMeshForTest(toCreateInRendererPointer)));

	MeshUniqueIdInRenderer meshId = OpenGLRendererToTest->CreateMesh(toCreateInRendererPointer);

	EXPECT_TRUE(OpenGLRendererToTest->IsMeshCreated(meshId));
}

TEST_F(OpenGLRendererTest, AllMeshesCreatedHaveUniqueIds)
{
	Mesh meshThatWillBeCreatedManyTimes = TestObjects::CreateTriangleMesh();
	std::vector<Mesh> meshesToCreate;
	for (int i = 0; i < 100; ++i)
	{
		meshesToCreate.push_back(meshThatWillBeCreatedManyTimes);
	}

	ON_CALL(*MockedOpenGL, CreateMesh(meshThatWillBeCreatedManyTimes))
		.WillByDefault(Return(CreateOpenGLMeshForTest(meshThatWillBeCreatedManyTimes)));

	std::vector<MeshUniqueIdInRenderer> createdMeshesIds;
	for (const Mesh& meshToCreate : meshesToCreate)
	{
		createdMeshesIds.push_back(OpenGLRendererToTest->CreateMesh(meshToCreate));
	}

	bool areAllMeshIdsUnique = true;

	std::unordered_set<MeshUniqueIdInRenderer, MeshUniqueIdInRendererHash> uniqueMeshIds;
	for (const MeshUniqueIdInRenderer& meshId : createdMeshesIds)
	{
		auto insertionResult = uniqueMeshIds.insert(meshId);
		if (!insertionResult.second)
		{
			areAllMeshIdsUnique = false;
			break;
		}
	}
	ASSERT_TRUE(areAllMeshIdsUnique);
}

TEST_F(OpenGLRendererTest, DrawingANonCreatedMeshThrowsException)
{
	MeshUniqueIdInRenderer meshIdNotInRenderer(5);
	ASSERT_THROW(OpenGLRendererToTest->DrawMesh(meshIdNotInRenderer), MeshIdNotFoundError);
}

TEST_F(OpenGLRendererTest, MeshWithMaterialCreatesATextureInRenderer)
{
	Mesh toCreateInRenderer = TestObjects::CreateTriangleMeshWithMaterial();

	OpenGLTexture createdTexture(1);
	EXPECT_CALL(*MockedOpenGL, CreateTexture(toCreateInRenderer.GetMaterial().GetTexture().get()))
		.WillOnce(Return(createdTexture));

	auto createdMeshId = OpenGLRendererToTest->CreateMesh(toCreateInRenderer);

	const OpenGLMesh& meshForId = OpenGLRendererToTest->GetCreatedMesh(createdMeshId);
	EXPECT_EQ(meshForId.GetTexture().get(), createdTexture);
}

TEST_F(OpenGLRendererTest, CreatedMeshCanBeDrawn)
{
	Mesh meshToCreate = TestObjects::CreateTriangleMesh();
	OpenGLMesh meshCreatedInOpenGL = CreateOpenGLMeshForTest(meshToCreate);

	// Not using EXPECT_CALL because this is already validated in a different test
	ON_CALL(*MockedOpenGL, CreateMesh(meshToCreate)).WillByDefault(Return(meshCreatedInOpenGL));
	EXPECT_CALL(*MockedOpenGL, DrawMesh(meshCreatedInOpenGL));

	MeshUniqueIdInRenderer createdMeshId = OpenGLRendererToTest->CreateMesh(meshToCreate);
	OpenGLRendererToTest->DrawMesh(createdMeshId);
}

TEST_F(OpenGLRendererTest, SetShaderAttachesItToTheProgramAndAddsItsUniformVariables)
{
	Shader shaderToSet = aVertexShader().WithSourceCode("test source code.").WithVariables(UniformVariables().With(aFloat().Named("testUniformVariableFloat"))).Build();

	OpenGLShader openGLShaderCreated(GL_VERTEX_SHADER,
		shaderToSet.GetSourceCode(),
		1,
		OpenGLShaderUtils::ConvertToOpenGL(shaderToSet.GetVariables()));

	{
		InSequence orderedExpectations;

		EXPECT_CALL(*MockedOpenGL, CreateShader(shaderToSet))
			.WillOnce(Return(openGLShaderCreated));

		EXPECT_CALL(*MockedOpenGL, CompileShader(openGLShaderCreated));
	}

	auto copyOfCurrentShaderProgram = *OpenGLRendererToTest->GetCurrentShaderProgram();
	EXPECT_CALL(*MockedOpenGL, AttachShaderToProgram(openGLShaderCreated, copyOfCurrentShaderProgram)).Times(1);

	OpenGLRendererToTest->SetShader(shaderToSet);

	EXPECT_TRUE(OpenGLRendererToTest->GetCurrentShaderProgram()->IsShaderAttached(openGLShaderCreated));
}