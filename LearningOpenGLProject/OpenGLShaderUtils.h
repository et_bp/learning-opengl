#pragma once

#include <string>
#include <GL/glew.h>
#include <vector>

#include "Shader.h"
#include "OpenGLUniformVariablesCollection.h"
#include "ShaderUniformVariableCollection.h"
#include "OpenGLUniformVariable.h"

namespace OpenGLShaderUtils
{
	GLenum ToGLenum(Shader::Type iShaderType);
	std::string GLShaderTypeToString(GLenum iShaderType);

	OpenGLUniformVariablesCollection ConvertToOpenGL(const ShaderUniformVariableCollection& iToConvert);

	template<typename OpenGLType, typename T>
	static OpenGLUniformVariableTyped<OpenGLType> ConvertToOpenGL(const ShaderUniformVariable<T>& iToConvert)
	{
		return OpenGLUniformVariableTyped<OpenGLType>(iToConvert.GetName(), iToConvert.GetValue());
	}

	template<typename OpenGLType, typename T>
	static std::vector<OpenGLUniformVariableTyped<OpenGLType>> ConvertToOpenGL(const std::vector<ShaderUniformVariable<T>>& iToConvert)
	{
		std::vector<OpenGLUniformVariableTyped<OpenGLType>> converted;
		for (const auto& variableToConvert : iToConvert)
		{
			converted.push_back(ConvertToOpenGL<OpenGLType, T>(variableToConvert));
		}
		return converted;
	}
}

