#include "Assimp3DSceneImporter.h"

#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>


#include <boost/format.hpp>

#include "Scene3D.h"
#include "Mesh.h"
#include "Texel.h"

Scene3D Assimp3DSceneImporter::ImportFromFile(const std::string& iFilePath) const
{
	Assimp::Importer importer;
	const aiScene* importedAssimpScene = importer.ReadFile(iFilePath, aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenSmoothNormals | aiProcess_JoinIdenticalVertices);

	if (!importedAssimpScene)
	{
		throw Import3DSceneError((boost::format("Error occurred when importing 3D scene from file \"%1%\". Error: \"%2%\"") % iFilePath % importer.GetErrorString()).str());
	}

	return CreateSceneFromAssimpScene(importedAssimpScene);
}

Scene3D Assimp3DSceneImporter::CreateSceneFromAssimpScene(const aiScene* iAssimpScene) const
{
	Scene3D createdSceneFromImport;

	CopyAssimpNodeContentToScene(iAssimpScene->mRootNode, iAssimpScene, createdSceneFromImport);

	return createdSceneFromImport;
}

void Assimp3DSceneImporter::CopyAssimpNodeContentToScene(aiNode* iAssimpNode, const aiScene* iAssimpSceneWithAllData, Scene3D& iCopyTo) const
{
	for (unsigned int i = 0; i < iAssimpNode->mNumMeshes; ++i)
	{
		aiMesh* meshFetchedFromAssimpScene = iAssimpSceneWithAllData->mMeshes[iAssimpNode->mMeshes[i]];
		CopyAssimpMeshToScene(meshFetchedFromAssimpScene, iCopyTo);
	}

	for (unsigned int i = 0; i < iAssimpNode->mNumChildren; ++i)
	{
		CopyAssimpNodeContentToScene(iAssimpNode->mChildren[i], iAssimpSceneWithAllData, iCopyTo);
	}
}

void Assimp3DSceneImporter::CopyAssimpMeshToScene(aiMesh* iAssimpMesh, Scene3D& iCopyTo) const
{
	iCopyTo.AddMesh(std::make_unique<Mesh>(GetIndices(iAssimpMesh), GetVertices(iAssimpMesh), GetNormals(iAssimpMesh), GetTextureMapping(iAssimpMesh)));
}

std::vector<unsigned int> Assimp3DSceneImporter::GetIndices(aiMesh* iFromAssimpMesh) const
{
	std::vector<unsigned int> indices;
	for (unsigned int i = 0; i < iFromAssimpMesh->mNumFaces; ++i)
	{
		aiFace assimpFace = iFromAssimpMesh->mFaces[i];
		for (unsigned int j = 0; j < assimpFace.mNumIndices; ++j)
		{
			indices.push_back(assimpFace.mIndices[j]);
		}
	}
	return indices;
}

std::vector<glm::vec3> Assimp3DSceneImporter::GetVertices(aiMesh* iFromAssimpMesh) const
{
	std::vector<glm::vec3> vertices;
	for (unsigned int i = 0; i < iFromAssimpMesh->mNumVertices; i++)
	{
		vertices.push_back(ConvertVector3(iFromAssimpMesh->mVertices[i]));
	}
	return vertices;
}

std::vector<glm::vec3> Assimp3DSceneImporter::GetNormals(aiMesh* iFromAssimpMesh) const
{
	std::vector<glm::vec3> normals;
	for (unsigned int i = 0; i < iFromAssimpMesh->mNumVertices; i++)
	{
		normals.push_back(-1.f * ConvertVector3(iFromAssimpMesh->mNormals[i]));
	}
	return normals;
}

std::vector<Texel> Assimp3DSceneImporter::GetTextureMapping(aiMesh* iFromAssimpMesh) const
{
	std::vector<Texel> textureMapping;

	static const Texel defaultTexelIfNoneDefined(0.f, 0.f);
	for (unsigned int i = 0; i < iFromAssimpMesh->mNumVertices; i++)
	{
		if (iFromAssimpMesh->HasTextureCoords(0))
		{
			// By default, use the first texture mapping supplied for this vertex
			aiVector3D textureCoordForVertex = iFromAssimpMesh->mTextureCoords[0][i];
			// Only use the first 2 coordinates of the 3DVector since we only support 2D textures at the moment
			textureMapping.push_back(Texel(textureCoordForVertex.x, textureCoordForVertex.y));
		}
		else
		{
			textureMapping.push_back(defaultTexelIfNoneDefined);
		}
	}

	return textureMapping;
}

glm::vec3 Assimp3DSceneImporter::ConvertVector3(const aiVector3D& iFromAssimpVector3) const
{
	return {iFromAssimpVector3.x, iFromAssimpVector3.y, iFromAssimpVector3.z};
}
