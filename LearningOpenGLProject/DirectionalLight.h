#pragma once

#include <glm/vec3.hpp>

#include "Light.h"

/**
 * Light source located very far from the objects (considered to be infinitely far) that lights the whole world with a unidirectional light.
 */
class DirectionalLight : public Light
{
public:
	DirectionalLight(float iIntensity, const ColorRGB& iColor, glm::vec3 iDirection)
		: Light(iIntensity, iColor)
		, mDirection(iDirection)
	{}

	glm::vec3 GetDirection() const { return mDirection; }

private:

	glm::vec3 mDirection;
};

