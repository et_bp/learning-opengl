#pragma once

#include "IWindowHandler.h"
#include "SoftwareVersion.h"
#include "IRunsInRealTime.h"

#include <string>
#include <memory>

struct GLFWwindow;
class IGlfwWindowEventsListener;
class ILogWriter;

class GlfwWindowHandler : public IWindowHandler, public IRunsInRealTime
{
public:
	GlfwWindowHandler(const SoftwareVersion& mOpenGLVersion, ILogWriter* iLog);
	~GlfwWindowHandler() override;

	// IWindowHandler overrides begin

	void Initialise() override;

	void CreateWindow(std::string iName, unsigned int iPixelWidth, unsigned int iPixelHeight) override;
	void CloseWindow() override;
	bool IsWindowAlive() const override;

	/*
	* Get the frame buffer of the current window.
	* The frame buffer is the part of the window that contains the rendering (without the frame of the window)
	*/
	Dimensions2D GetWindowViewportDimensions() const override;

	void ShowViewportChangesInWindow() override;

	// IWindowHandler overrides end

	// IRunsInRealTime overrides begin

	void Tick(float iDeltaSeconds) override;
	
	// IRunsInRealTime overrides end

	GLFWwindow* GetCurrentWindow() const { return mCurrentWindow; }

	void SetListener(std::shared_ptr<IGlfwWindowEventsListener> iListener) { mEventListener = iListener; }

private:

	void BroadcastWillPollEvents();
	void BroadcastCreatedWindow(GLFWwindow* iCreatedWindow);

	std::shared_ptr<IGlfwWindowEventsListener> mEventListener;

	SoftwareVersion mOpenGLVersion;
	GLFWwindow* mCurrentWindow = nullptr;

	ILogWriter* mLog;
};