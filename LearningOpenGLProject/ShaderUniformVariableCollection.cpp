#include "ShaderUniformVariableCollection.h"

ShaderUniformVariableCollection::ShaderUniformVariableCollection(const std::vector<ShaderUniformVariable<float>>& iFloatUniformVariables, const std::vector<ShaderUniformVariable<glm::mat4>>& iMatricesUniformVariables, const std::vector<ShaderUniformVariable<glm::vec3>>& iVector3UniformVariables)
	: mFloatUniformVariables(iFloatUniformVariables)
	, mMatrices4UniformVariables(iMatricesUniformVariables)
	, mVector3UniformVariables(iVector3UniformVariables)
{

}

ShaderUniformVariableCollection::ShaderUniformVariableCollection(const std::vector<ShaderUniformVariableCollection>& iCollectionsToCombine)
{
	for (const auto& collection : iCollectionsToCombine)
	{
		Add(collection);
	}
}

void ShaderUniformVariableCollection::Add(ShaderUniformVariableCollection iOtherCollection)
{
	Add<float>(iOtherCollection.GetVariables<float>());
	Add<glm::mat4>(iOtherCollection.GetVariables<glm::mat4>());
	Add<glm::vec3>(iOtherCollection.GetVariables<glm::vec3>());
}

bool ShaderUniformVariableCollection::operator==(const ShaderUniformVariableCollection& iOther) const
{
	return
		mFloatUniformVariables == iOther.mFloatUniformVariables &&
		mMatrices4UniformVariables == iOther.mMatrices4UniformVariables &&
		mVector3UniformVariables == iOther.mVector3UniformVariables;
}
