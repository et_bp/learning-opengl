#pragma once
#include <GL/glew.h>
#include <glm/vec3.hpp>
#include <vector>
#include <boost/optional.hpp>

#include "OpenGLTexture.h"

class OpenGLMesh
{
public:

	OpenGLMesh() = default;
	OpenGLMesh(const std::vector<GLuint>& iIndices, const std::vector<glm::vec3>& iVertices, const std::vector<glm::vec3> iNormals, GLuint iVAO, GLuint iIBO, GLuint iVBO)
		: mIndices(iIndices), mVertices(iVertices), mNormals(iNormals), mVAO(iVAO), mIBO(iIBO), mVBO(iVBO)
	{

	}

	bool operator==(const OpenGLMesh& iOther) const;

	const std::vector<GLuint>& GetIndices() const { return mIndices; }

	const std::vector<glm::vec3>& GetVertices() const { return mVertices; }
	GLuint GetNumberOfVertices() const { return mIndices.size(); }

	const std::vector<glm::vec3>& GetNormals() const { return mNormals; }

	GLuint GetVAO() const { return mVAO; }
	GLuint GetIBO() const { return mIBO; }

	void SetTexture(const OpenGLTexture& iTexture) { mTexture = iTexture; }
	const boost::optional<OpenGLTexture>& GetTexture() const { return mTexture; }

private:

	boost::optional<OpenGLTexture> mTexture;

	std::vector<GLuint> mIndices;
	std::vector<glm::vec3> mVertices;
	std::vector<glm::vec3> mNormals;
	GLuint mVAO = 0;
	GLuint mIBO = 0;
	GLuint mVBO = 0;	
};

