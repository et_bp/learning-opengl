#include "OpenGLUniformVariablesCollection.h"

OpenGLUniformVariablesCollection::OpenGLUniformVariablesCollection(const std::vector<OpenGLUniformVariableTyped<GLfloat>>& iFloats)
	: mFloats(iFloats)
{

}

void OpenGLUniformVariablesCollection::Add(const OpenGLUniformVariablesCollection& iVariablesToAdd)
{
	Add(iVariablesToAdd.GetVariables<GLfloat>());
	Add(iVariablesToAdd.GetVariables<glm::mat4>());
	Add(iVariablesToAdd.GetVariables<glm::vec3>());
}

std::vector<OpenGLUniformVariable*> OpenGLUniformVariablesCollection::GetAll()
{
	std::vector<OpenGLUniformVariable*> allVariables;

	for (GLuint i = 0; i < mFloats.size(); ++i)
	{
		allVariables.push_back(&mFloats[i]);
	}

	for (GLuint i = 0; i < mMatrices4.size(); ++i)
	{
		allVariables.push_back(&mMatrices4[i]);
	}

	for (GLuint i = 0; i < mVectors3.size(); ++i)
	{
		allVariables.push_back(&mVectors3[i]);
	}

	return allVariables;
}

std::ostream& operator<<(std::ostream& iOutStream, const OpenGLUniformVariablesCollection& iVariablesCollection)
{

	iOutStream << "{ ";

	if (!iVariablesCollection.mFloats.empty())
	{
		iOutStream << "Floats = " << iVariablesCollection.mFloats << ";";
	}

	if (!iVariablesCollection.mMatrices4.empty())
	{
		iOutStream << "Matrices = " << iVariablesCollection.mMatrices4 << ";";
	}

	if (!iVariablesCollection.mVectors3.empty())
	{
		iOutStream << "Vectors = " << iVariablesCollection.mVectors3 << ";";
	}

	iOutStream << " }";

	return iOutStream;
}

bool OpenGLUniformVariablesCollection::operator==(const OpenGLUniformVariablesCollection& iOther) const
{
	return
		mFloats == iOther.mFloats &&
		mMatrices4 == iOther.mMatrices4 &&
		mVectors3 == iOther.mVectors3;
}
