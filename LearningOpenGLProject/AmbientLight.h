#pragma once
#include "Light.h"

/**
 * Light that should be applied on every faces of everything in the world equally.
 */
class AmbientLight : public Light
{
public:
	AmbientLight(float iIntensity, const ColorRGB& iColor)
		: Light(iIntensity, iColor)
	{}
};

