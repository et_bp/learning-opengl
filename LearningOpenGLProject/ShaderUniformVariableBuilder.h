#pragma once
#include "ShaderUniformVariable.h"


template<typename T>
class ShaderUniformVariableBuilder
{
public:
	ShaderUniformVariable<T> Build() const
	{
		return ShaderUniformVariable<T>(mName, mValue);
	}

	ShaderUniformVariableBuilder& Named(const std::string& iName) { mName = iName; return *this; }
	ShaderUniformVariableBuilder& WithValue(const T iValue) { mValue = iValue; return *this; }

private:
	std::string mName;
	T mValue;
};
