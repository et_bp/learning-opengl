#include "Shader.h"



bool Shader::operator==(const Shader& iOther) const
{
	return
		mType == iOther.GetType() &&
		mSourceCode == iOther.mSourceCode &&
		mVariables == iOther.mVariables;
}

std::ostream& operator<<(std::ostream& iOutStream, const Shader& iToStream)
{
	iOutStream << iToStream.GetType() << " shader";
	return iOutStream;
}

std::ostream& operator<<(std::ostream& iOutStream, Shader::Type iToStream)
{
	switch (iToStream)
	{
	case Shader::Type::Vertex:
		iOutStream << "Vertex";
		break;
	case Shader::Type::Fragment:
		iOutStream << "Fragment";
		break;
	default:
		BOOST_ASSERT_MSG(false, "Shader type does not have any text conversion. Please add one.");
		break;
	}

	return iOutStream;
}
