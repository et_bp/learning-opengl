#pragma once
#include <string>

template<typename T>
class ShaderUniformVariable
{
public:
	ShaderUniformVariable() = default;
	ShaderUniformVariable(const std::string& iName, T iValue) : mName(iName), mValue(iValue) {}

	const std::string& GetName() const { return mName; }
	const T& GetValue() const { return mValue; }

	bool operator==(const ShaderUniformVariable<T>& iOther) const;

private:
	std::string mName;
	T mValue;
};

template<typename T>
bool ShaderUniformVariable<T>::operator==(const ShaderUniformVariable<T>& iOther) const
{
	return mName == iOther.mName && mValue == iOther.mValue;
}
