#pragma once

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

struct AircraftRotation;
struct CameraMovement;

/**
 * Position and rotation of a player camera
 */
class PlayerCamera
{
public:
	PlayerCamera(const glm::vec3& iInitialPosition, const AircraftRotation& iInitialRotation);

	/**
	 * Move the camera by first rotating it, then applying the translation (a.k.a movement)
	 */
	void Move(const AircraftRotation& iRotation, const CameraMovement& iMovement);

	glm::mat4 ComputeViewMatrix();

	glm::vec3 GetPosition() { return mPosition; }

private:

	/**
	 * Update orientation of the camera based on the current aircraft rotation
	 * Orientation is defined by the front and right vectors
	 */
	void UpdateOrientationFromRotation();

	void MovePosition(const CameraMovement& iMovement);

	glm::vec3 mPosition = { 0.f, 0.f, 0.f };

	glm::vec3 mFront = { 0.f, 0.f, 0.f };
	glm::vec3 mRight = { 0.f, 0.f, 0.f };
	glm::vec3 mUp = { 0.f, 0.f, 0.f };

	// Yaw in degrees
	float mYaw = -90.f;
	// Pitch in degrees
	float mPitch = 0.f;
};

