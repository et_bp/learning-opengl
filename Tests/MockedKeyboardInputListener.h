#pragma once

#include <gmock/gmock.h>

#include "IKeyboardInputListener.h"
#include "Keyboard.h"


class MockedKeyboardInputListener : public IKeyboardInputListener
{
public:
	MOCK_METHOD(bool, IsPressed, (Keyboard::Key), (const, override));
	MOCK_METHOD(void, When, (Keyboard::Action, Keyboard::Key, KeyEventCallback), (override));
};

