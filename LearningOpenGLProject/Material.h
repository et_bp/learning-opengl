#pragma once

#include <memory>

struct Image;

/**
 * Visual properties of the surface of a mesh. 
 * For example, defines the colors of the surface as well as the way it reacts to lights.
 */
class Material
{
public:

	Material() = default;
	Material(std::shared_ptr<Image> iTexture, float iSpecularIntensity, float iSpecularPrecision);

	bool operator==(const Material& iOther) const;

	bool IsValid() const { return mTexture.get() != nullptr; }

	std::shared_ptr<Image> GetTexture() const { return mTexture; }
	float GetSpecularIntensity() const { return mSpecularIntensity; }
	float GetSpecularPrecision() const { return mSpecularPrecision; }

private:
	
	/**
	 * How much light the specular reflection will add to the surface
	 * Should be a value between 0 and 1. 0 means it will add nothing, 1 means it will add a lot of light.
	 */
	float mSpecularIntensity = 0.3f;

	/**
	 * How spread the specular reflection will be. Should be a value that is a power of 2.
	 * The higher the value, the more the reflection will show only around the perfect reflection to the camera
	 * Perfect reflection means that the incoming light bounces directly to the camera
	 */
	float mSpecularPrecision = 8.f;

	/**
	 * Reference to the texture used by this material
	 * It doesn't own the texture, but it keeps a hard reference to it
	 */
	std::shared_ptr<Image> mTexture;
};

