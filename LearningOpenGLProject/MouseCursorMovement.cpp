#include "MouseCursorMovement.h"

#include <boost/format.hpp>

std::string MouseCursorMovement::ToString()
{

	return (boost::format("Mouse moved from %1% to %2% (Delta is (%3%, %4%)) ")
		% FromPosition.ToString()
		% ToPosition.ToString()
		% GetDelta().x % GetDelta().y
		).str();
}
