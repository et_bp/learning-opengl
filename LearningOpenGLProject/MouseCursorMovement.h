#pragma once

#include <glm/glm.hpp>
#include <string>

#include "MouseCursorPosition.h"

struct MouseCursorMovement
{
	MouseCursorMovement() = default;
	MouseCursorMovement(const MouseCursorPosition& iFrom, const MouseCursorPosition& iTo) : FromPosition(iFrom), ToPosition(iTo) {}

	std::string ToString();

	glm::vec2 GetDelta() { return ToPosition.CursorPosition - FromPosition.CursorPosition; }

	MouseCursorPosition FromPosition;
	MouseCursorPosition ToPosition;
};