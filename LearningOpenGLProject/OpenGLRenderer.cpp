#include "OpenGLRenderer.h"

#include <GL/glew.h>
#include <stdexcept>
#include <boost/format.hpp>
#include <boost/assert.hpp>

#include "Dimensions2D.h"
#include "ColorRGBA.h"
#include "Mesh.h"
#include "MeshUniqueIdInRenderer.h"

#include "OpenGLShader.h"
#include "OpenGLShaderUtils.h"
#include "IOpenGLExtensionsHandler.h"
#include "IOpenGLLibrary.h"

#include "ILogWriter.h"
#include "OpenGLTexture.h"
#include "Image.h"

OpenGLRenderer::OpenGLRenderer(IOpenGLLibrary* iOpenGL, IOpenGLExtensionsHandler* iExtensionHandler, ILogWriter* iLogWriter)
	: mOpenGL(iOpenGL)
	, mLog(iLogWriter)
	, mExtensionHandler(iExtensionHandler)
{
}

void OpenGLRenderer::Initialise(const Dimensions2D& iViewportDimensions)
{
	mLog->Write("Starting OpenGL initialization...");

	mExtensionHandler->Initialize();

	mOpenGL->EnableCapability(GL_DEPTH_TEST);

	mLog->Write((boost::format("Setting up OpenGL viewport with resolution %1% x %2%.") % iViewportDimensions.Width % iViewportDimensions.Height).str());

	mOpenGL->SetViewportDimensions(iViewportDimensions);

	try
	{
		mShaderProgram = mOpenGL->CreateShaderProgram();
		mLog->Write((boost::format("Created %1%.") % mShaderProgram.get()).str());
	}
	catch (const std::runtime_error& e)
	{
		throw e;
	}
}

void OpenGLRenderer::SetBackgroundColor(const ColorRGBA& iColor)
{
	mOpenGL->ClearViewport(iColor);
}

void OpenGLRenderer::DrawMesh(const MeshUniqueIdInRenderer& iToDraw)
{
	if (!IsShaderProgramInitialized())
	{
		throw std::runtime_error("Failed to render viewport since the OpenGL renderer is not initialized.");
	}

	const OpenGLMesh& mesh = GetCreatedMesh(iToDraw);

	UpdateUniformVariablesValues(*mShaderProgram);

	mOpenGL->DrawMesh(mesh);
}

void OpenGLRenderer::SetShader(const Shader& iShader)
{
	if (!IsShaderProgramInitialized())
	{
		throw std::runtime_error("Trying to set a shader on a OpenGL renderer that is not initialized.");
	}
	
	try
	{
		OpenGLShader newShader = mOpenGL->CreateShader(iShader);
		mOpenGL->CompileShader(newShader);
		mLog->Write((boost::format("%1% compiled successfully with uniform variables %2%.") % newShader % newShader.GetUniformVariables()).str());

		mOpenGL->AttachShaderToProgram(newShader, *mShaderProgram);
		mShaderProgram->AttachShader(newShader);

		mLog->Write((boost::format("%1% is attached to %2%.") % newShader % mShaderProgram.get()).str());
	}
	catch (const std::runtime_error & e)
	{
		printf("%s\n", e.what());
	}
}

void OpenGLRenderer::AllShadersAreSet()
{
	if (!IsShaderProgramInitialized())
	{
		throw std::runtime_error("Trying to link a shader program on an OpenGL renderer that is not initialized.");
	}

	mOpenGL->LinkShaderProgram(*mShaderProgram);
	mOpenGL->ValidateShaderProgram(*mShaderProgram);

	mOpenGL->BindUniformVariablesInProgram(*mShaderProgram);

	mOpenGL->UseShaderProgram(*mShaderProgram);
}

MeshUniqueIdInRenderer OpenGLRenderer::CreateMesh(const Mesh& iToAdd)
{
	OpenGLMesh createdOpenGLMesh = mOpenGL->CreateMesh(iToAdd);

	std::shared_ptr<Image> textureOnMesh = iToAdd.GetMaterial().GetTexture();
	if (textureOnMesh)
	{
		createdOpenGLMesh.SetTexture(mOpenGL->CreateTexture(textureOnMesh.get()));
	}

	auto idForCreatedMesh = GetNextMeshId();
	mCreatedMeshes.insert({ idForCreatedMesh, createdOpenGLMesh });

	return idForCreatedMesh;
}

bool OpenGLRenderer::IsMeshCreated(const MeshUniqueIdInRenderer& iMeshId) const
{
	return mCreatedMeshes.find(iMeshId) != mCreatedMeshes.end();
}

void OpenGLRenderer::SetValueInUniformVariable(const std::string& iVariableName, float iNewValue)
{
	mShaderProgram->SetValueInUniformVariable(OpenGLShaderUtils::ConvertToOpenGL<GLfloat, float>({iVariableName, iNewValue}));
}

void OpenGLRenderer::SetValueInUniformVariable(const std::string& iVariableName, const glm::mat4& iNewValue)
{
	mShaderProgram->SetValueInUniformVariable(OpenGLShaderUtils::ConvertToOpenGL<glm::mat4, glm::mat4>({ iVariableName, iNewValue }));
}

void OpenGLRenderer::SetValueInUniformVariable(const std::string& iVariableName, const glm::vec3& iNewValue)
{
	mShaderProgram->SetValueInUniformVariable(OpenGLShaderUtils::ConvertToOpenGL<glm::vec3, glm::vec3>({ iVariableName, iNewValue }));
}

const OpenGLShaderProgram* OpenGLRenderer::GetCurrentShaderProgram()
{
	if (mShaderProgram.has_value())
	{
		return mShaderProgram.get_ptr();
	}
	else
	{
		return nullptr;
	}
}

const OpenGLMesh& OpenGLRenderer::GetCreatedMesh(const MeshUniqueIdInRenderer& iMeshId) const
{
	auto searchResult = mCreatedMeshes.find(iMeshId);
	if (searchResult != mCreatedMeshes.end())
	{
		return searchResult->second;
	}
	else
	{
		throw MeshIdNotFoundError((boost::format("Couldn't find any Mesh with Id: %1%") % iMeshId).str());
	}
}

bool OpenGLRenderer::IsShaderProgramInitialized()
{
	return mShaderProgram.has_value();
}

void OpenGLRenderer::UpdateUniformVariablesValues(const OpenGLShaderProgram& iProgram)
{
	mOpenGL->SetUniformVariables(iProgram.GetUniformVariablesCollection().GetVariables<GLfloat>());
	mOpenGL->SetUniformVariables(iProgram.GetUniformVariablesCollection().GetVariables<glm::mat4>());
	mOpenGL->SetUniformVariables(iProgram.GetUniformVariablesCollection().GetVariables<glm::vec3>());
}

MeshUniqueIdInRenderer OpenGLRenderer::GetNextMeshId()
{
	unsigned int idToReturn = mLastUsedId;
	mLastUsedId++;

	BOOST_ASSERT_MSG(mLastUsedId < UINT_MAX, 
		"The id used to identify the meshes has reached the maximum number that can be contained in an unsigned int. Could probably be fixed by recycling previously used IDs if they are not used anymore.");

	return idToReturn;
}
