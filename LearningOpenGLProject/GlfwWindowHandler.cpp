#include "GlfwWindowHandler.h"
#include "Dimensions2D.h"
#include "IGlfwWindowEventsListener.h"
#include "ILogWriter.h"

#include <GLFW/glfw3.h>
#include <stdexcept>
#include <boost/format.hpp>

GlfwWindowHandler::GlfwWindowHandler(const SoftwareVersion& iOpenGLVersion, ILogWriter* iLog)
	: mOpenGLVersion(iOpenGLVersion)
	, mLog(iLog)
{

}

GlfwWindowHandler::~GlfwWindowHandler()
{
    if (mCurrentWindow)
    {
        glfwDestroyWindow(mCurrentWindow);
    }
    glfwTerminate();
}

void GlfwWindowHandler::Initialise()
{
    if (!glfwInit())
    {
        throw std::runtime_error("Failed to initialize the GLFW framework.");
    }

	mLog->Write((boost::format("GLFW initialized with OpenGL version %1%.%2%") % mOpenGLVersion.MinorVersion % mOpenGLVersion.MinorVersion).str());

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, mOpenGLVersion.MajorVersion);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, mOpenGLVersion.MinorVersion);

    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
}

void GlfwWindowHandler::Tick(float iDeltaSeconds)
{
	BroadcastWillPollEvents();
	glfwPollEvents();
}

void GlfwWindowHandler::CreateWindow(std::string iName, unsigned int iPixelWidth, unsigned int iPixelHeight)
{
    GLFWwindow* mainWindow = glfwCreateWindow(iPixelWidth, iPixelHeight, iName.c_str(), nullptr, nullptr);
	if (!mainWindow)
    {
        throw std::runtime_error(std::string("GLFW failed to create a window named %s", iName.c_str()));
    }

    // Set window as context for OpenGL
	glfwMakeContextCurrent(mainWindow);
    mCurrentWindow = mainWindow;

	mLog->Write((boost::format("GLFW created window named \"%1%\" with resolution %2% x %3%") % iName % iPixelWidth % iPixelHeight).str());

	BroadcastCreatedWindow(mainWindow);
}

void GlfwWindowHandler::CloseWindow()
{
	glfwSetWindowShouldClose(mCurrentWindow, true);
	mCurrentWindow = nullptr;
}

bool GlfwWindowHandler::IsWindowAlive() const
{
	if (!mCurrentWindow)
	{
		return false;
	}

	return !glfwWindowShouldClose(mCurrentWindow);
}
Dimensions2D GlfwWindowHandler::GetWindowViewportDimensions() const
{
	Dimensions2D windowViewportDimensions;
    glfwGetFramebufferSize(mCurrentWindow, &windowViewportDimensions.Width, &windowViewportDimensions.Height);
    return windowViewportDimensions;
}

void GlfwWindowHandler::ShowViewportChangesInWindow()
{
	if (!mCurrentWindow)
	{
		return;
	}

	glfwSwapBuffers(mCurrentWindow);
}

void GlfwWindowHandler::BroadcastWillPollEvents()
{
	if (mEventListener)
	{
		mEventListener->BeforePollWindowEvents();
	}
}

void GlfwWindowHandler::BroadcastCreatedWindow(GLFWwindow* iCreatedWindow)
{
	if (mEventListener)
	{
		mEventListener->OnWindowInitialized(iCreatedWindow);
	}
}
