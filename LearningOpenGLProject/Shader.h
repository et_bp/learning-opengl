#pragma once
#include <string>

#include <iostream>
#include <boost/assert.hpp>

#include "ShaderUniformVariableCollection.h"

struct Shader
{
public:

	enum class Type
	{
		Vertex,
		Fragment
	};

	Shader(Type iType, const std::string& iSourceCode, const ShaderUniformVariableCollection& iVariables) : mType(iType), mSourceCode(iSourceCode), mVariables(iVariables) {}

	Type GetType() const
	{
		return mType;
	}

	std::string GetSourceCode() const
	{
		return mSourceCode;
	}

	ShaderUniformVariableCollection GetVariables() const { return mVariables; }

	bool operator==(const Shader& iOther) const;

	friend std::ostream& operator<<(std::ostream& iOutStream, const Shader& iToStream);

	friend std::ostream& operator<<(std::ostream& iOutStream, Shader::Type iToStream);
private:

	Type mType;
	std::string mSourceCode;
	ShaderUniformVariableCollection mVariables;
};