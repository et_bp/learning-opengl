#pragma once

#include <string>

class IFileReader
{
public:
	virtual std::string ReadFile(const std::string& iPathRelativeToSolution) = 0;
};

