#pragma once
#include <gmock/gmock.h>

#include "IOpenGLLibrary.h"
#include "ColorRGBA.h"
#include "OpenGLTexture.h"
#include "Texel.h"

class MockedOpenGLLibrary : public IOpenGLLibrary
{
public:
	MOCK_METHOD(void, EnableCapability, (GLenum iCapability), (override));
	MOCK_METHOD(void, SetViewportDimensions, (const Dimensions2D& iDimensions), (override));

	MOCK_METHOD(void, ClearViewport, (const ColorRGBA& iWithColor), (override));

	MOCK_METHOD(OpenGLTexture, CreateTexture, (const Image* const iFromImage), (override));

	MOCK_METHOD(OpenGLMesh, CreateMesh, (const Mesh& iFromMesh), (override));
	MOCK_METHOD(void, DrawMesh, (const OpenGLMesh& iToDraw), (override));

	MOCK_METHOD(OpenGLShader, CreateShader, (const Shader& iFromShader), (const, override));
	MOCK_METHOD(void ,CompileShader, (const OpenGLShader& iToCompile), (const, override));

	MOCK_METHOD(void, SetUniformVariable, (const OpenGLUniformVariableTyped<GLfloat>& iFloat), (override));
	MOCK_METHOD(void, SetUniformVariable, (const OpenGLUniformVariableTyped<glm::mat4>& iMatrix4), (override));
	MOCK_METHOD(void, SetUniformVariable, (const OpenGLUniformVariableTyped<glm::vec3>& iVector3), (override));
	
	MOCK_METHOD(OpenGLShaderProgram, CreateShaderProgram, (), (override));
	MOCK_METHOD(void, UseShaderProgram, (const OpenGLShaderProgram& iToUse), (override));
	MOCK_METHOD(void, ClearShaderProgram, (), (override));
	MOCK_METHOD(void, LinkShaderProgram, (const OpenGLShaderProgram& iToLink));
	MOCK_METHOD(void, ValidateShaderProgram, (const OpenGLShaderProgram& iToValidate), (override));

	MOCK_METHOD(void, AttachShaderToProgram, (const OpenGLShader& iShader, OpenGLShaderProgram& ioAttachTo), (override));

	MOCK_METHOD(void, BindUniformVariablesInProgram, (OpenGLShaderProgram& ioProgram), (override));
};
