#pragma once

#include <GL/glew.h>
#include <vector>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

#include "OpenGLUniformVariable.h"

struct Dimensions2D;
class OpenGLShaderProgram;
struct ColorRGBA;
class OpenGLMesh;
struct OpenGLShader;
struct Shader;
class OpenGLTexture;
struct Image;
struct Texel;
class Mesh;

/**
 * Exposes the feature of the OpenGL API, but with types and methods that wrap this API.
 */
class IOpenGLLibrary
{
public:
	virtual void EnableCapability(GLenum iCapability) = 0;
	
	virtual void SetViewportDimensions(const Dimensions2D& iDimensions) = 0;

	/** Clear color buffer with specified color and depth buffer. */
	virtual void ClearViewport(const ColorRGBA& iWithColor) = 0;

	/************************************************************************/
	/* Textures                                                             */
	/************************************************************************/

	virtual OpenGLTexture CreateTexture(const Image* const iFromImage) = 0;

	/************************************************************************/
	/* Meshes                                                               */
	/************************************************************************/

	/**
	 * Creates a mesh in OpenGL.
	 *
	 * @param iIndices Indices of the mesh. Each index in this list refers to a vertex in the iVertices list. Indexing is used to reuse a vertex for multiple triangles on the mesh.
	 * @param iVertices Vertices of the mesh.
	 * @param iNormals Normals for each vertices of the mesh.
	 * @param iTextureMapping The way the texels of a texture will be mapped on the vertices. There needs to be one texel defined for each vertex of the mesh.
	 * 
	 * @returns The mesh created in OpenGL with the unique ID of it.
	 *
	 * @throws invalid_argument exceptions if there are normals defined, but there are more or less than the vertices.
	 * @throws invalid_argument exceptions if there is a texture mapping defined and the number of texels in the texture mapping doesn't match the amount of vertices.
	 */
	virtual OpenGLMesh CreateMesh(const Mesh& iFromMesh) = 0;

	/**
	 * Draw the provided mesh. If the mesh contains a texture, the texture will be applied on the mesh using the texture mapping defined in it.
	 */
	virtual void DrawMesh(const OpenGLMesh& iToDraw) = 0;

	/************************************************************************/
	/* Shaders                                                              */
	/************************************************************************/

	virtual OpenGLShader CreateShader(const Shader& iFromShader) const = 0;
	virtual void CompileShader(const OpenGLShader& iToCompile) const = 0;

	/************************************************************************/
	/* Shader Uniform Variables                                             */
	/************************************************************************/

	template<typename T>
	void SetUniformVariables(const std::vector<OpenGLUniformVariableTyped<T>>& iUniformVariables)
	{
		for each (const auto& uniformVariable in iUniformVariables)
		{
			SetUniformVariable(uniformVariable);
		}
	}

	virtual void SetUniformVariable(const OpenGLUniformVariableTyped<GLfloat>& iFloat) = 0;
	virtual void SetUniformVariable(const OpenGLUniformVariableTyped<glm::mat4>& iMatrix4) = 0;
	virtual void SetUniformVariable(const OpenGLUniformVariableTyped<glm::vec3>& iVector3) = 0;

	/************************************************************************/
	/* Shader Program                                                       */
	/************************************************************************/

	virtual OpenGLShaderProgram CreateShaderProgram() = 0;
	virtual void UseShaderProgram(const OpenGLShaderProgram& iToUse) = 0;
	/** Clear the last used shader program. */
	virtual void ClearShaderProgram() = 0;
	/** Linking the program will transfer vertex and fragment shaders to their respective processors */
	virtual void LinkShaderProgram(const OpenGLShaderProgram& iToLink) = 0;
	/** Validate the program makes sure the program can be run in the current OpenGL state */
	virtual void ValidateShaderProgram(const OpenGLShaderProgram& iToValidate) = 0;

	virtual void AttachShaderToProgram(const OpenGLShader& iShader, OpenGLShaderProgram& ioAttachTo) = 0;

	/**
	 * Bind the uniform variables stored in this program.
	 * From OpenGL perspective, the variables will be bound to the program as well.
	 * The resulting locations will be stored in the variables in the program object.
	 */
	virtual void BindUniformVariablesInProgram(OpenGLShaderProgram& ioProgram) = 0;
};
