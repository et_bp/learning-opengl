#include "OpenGLShaderProgram.h"

#include <algorithm>

#include "OpenGLShader.h"

OpenGLShaderProgram::OpenGLShaderProgram(GLuint iID)
	: mID(iID)
{

}

void OpenGLShaderProgram::AttachShader(const OpenGLShader& iShaderToAttach)
{
	AddUniformVariables(iShaderToAttach.GetUniformVariables());
	mShadersAttached.push_back(iShaderToAttach);
}

bool OpenGLShaderProgram::IsShaderAttached(const OpenGLShader& iShader) const
{
	return std::find(mShadersAttached.begin(), mShadersAttached.end(), iShader) != std::end(mShadersAttached);
}

bool OpenGLShaderProgram::operator==(const OpenGLShaderProgram& iOther) const
{
	return mID == iOther.mID && mShadersAttached == iOther.mShadersAttached && mShadersUniformVariables == iOther.mShadersUniformVariables;
}

void OpenGLShaderProgram::AddUniformVariables(const OpenGLUniformVariablesCollection& iUniformVariables)
{
	mShadersUniformVariables.Add(iUniformVariables);
}
