#pragma once

#include <stdexcept>
#include <memory>
#include <vector>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

class Mesh;
struct Image;

class InvalidMeshError : std::runtime_error
{
public:
	InvalidMeshError(const std::string& iWhat)
		: std::runtime_error(iWhat)
	{}
};

/**
 * Render a world containing meshes, the player, etc.
 */
class IWorldRenderer
{
public:

	/**
	 * Update the current player camera
	 * @param iPosition: Where the camera is located in world space
	 * @param iViewMatrix: Where the camera is looking
	 */
	virtual void UpdatePlayerCamera(const glm::vec3& iPosition, const glm::mat4x4& iViewMatrix) = 0;

	/**
	 * Set the projection matrix used to determine how the world will be seen through the camera
	 */
	virtual void SetProjectionMatrix(const glm::mat4x4& iNewProjectionMatrix) = 0;

	/**
	 * Set the model matrix used to determine the location, rotation and scale of what will be rendered
	 */
	virtual void SetModelmatrix(const glm::mat4x4& iNewModelMatrix) = 0;

	/**
	 * Add the mesh to the world so it will be rendered on the next Render call.
	 * @param iMeshToAdd: Mesh that will be added to the world. 
	 *                       Passed as a unique_ptr because the world will take ownership of that mesh.
	 *                       This could be changed in the future if the mesh needs to be used by various different objects.
	 * 
	 * Throws InvalidMeshError if the Mesh could not be added to the world it contains invalid informations
	 */
	virtual void AddMeshToWorld(std::unique_ptr<Mesh>&& iMeshToAdd) = 0;

	/**
	 * Add the meshes to the world so they will be rendered on the next Render call.
	 * @param iMeshesToAdd: Meshes that will be added to the world.
	 *					    Passed as r-value (&&) since the meshes are transfered into the world and owned by it.
	 *						If some of the meshes fail to be added, they will not be transfered and will stay in the array passed as parameter.
	 * 
	 */
	virtual void AddMeshesToWorld(std::vector<std::unique_ptr<Mesh>>&& iMeshesToAdd) = 0;
	
	virtual void Render() = 0;
};

