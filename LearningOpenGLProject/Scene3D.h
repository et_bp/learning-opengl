#pragma once
#include "Mesh.h"
#include <memory>

class Scene3D
{
public:
	Scene3D() = default;

	void AddMesh(std::unique_ptr<Mesh>&& iToAdd) { mMeshes.push_back(std::move(iToAdd)); }

	const std::vector<std::unique_ptr<Mesh>>& GetMeshes() const { return mMeshes; }
	std::vector<std::unique_ptr<Mesh>>& GetMeshes() { return mMeshes; }

	void RemoveAllMeshes() { mMeshes.clear(); }

private:

	std::vector<std::unique_ptr<Mesh>> mMeshes;
};

