# Learning OpenGL Project
*Developed by Etienne Beauchemin-Pepin*

![Project Cover Image](/Readme/LearningOpenGLCover.png)

## Goal of this project
The following are the main reasons why this project is being developed in order of importance.

1. As the title implies, I wanted to learn more about OpenGL and graphics programming. I'm using multiple resources to develop this project, but the main one is this [OpenGL programming class on *Udemy*](https://www.udemy.com/course/graphics-with-modern-opengl/).
1. Experiment with *GoogleTest*, *GoogleMock* and test driven development in general. To allow unit testing, this project is developed following the [dependency inversion pricinciple](https://stackify.com/dependency-inversion-principle/) for all classes.
1. Create clean, readable code.
1. Improve my knowledge of `std` and `boost` libraries as well as project setup in *Visual Studio*.

## What is currently implemented in this project?
The following are the features currently implemented in this project.

* Flying camera and controls for the player to move around. (using `wasd` keys and mouse)
* Import 3D scenes using the *Assimp* library. Does not support importing textures from the 3D scene yet.
* Phong lighting model:
    * Ambient lighting
    * Directional lighting
    * Specular lighting
* Import images to use as texture on the objects.
* Unit tests covering some of the main features of the project.

## How to develop code in this project
### Which IDE to use? 

Use [Visual Studio 2017](https://docs.microsoft.com/en-us/visualstudio/releasenotes/vs2017-relnotes).

We want to switch to *Visual Studio 2019*, the only reason keeping us from doing this is the [C++ Debugger Visualizers](https://marketplace.visualstudio.com/items?itemName=ArkadyShapkin.CDebuggerVisualizersforVS2017) plugin mentioned in *Any recommened plugin*. It isn't currently compatible with *Visual Studio 2019*.
### Any recommended plugins?

[C++ Debugger Visualizers](https://marketplace.visualstudio.com/items?itemName=ArkadyShapkin.CDebuggerVisualizersforVS2017): Since this project uses the *BOOST* library, it is recommenced to use this plugin in order to debug certain types.

### Steps to start coding

These steps assume that you have already pulled the project and opened the *.sln* in *Visual Studio*.

1. `Right+click` on the solution, and **click on `Restore NuGet Packages`**. This will download and install all the dependencies of this project (see the list of those in *Which dependencies are used in this project?* section)
1. Make sure your ***Solution Configurations* is set to `Debug` and your *Solution Platforms* to `x86`**. There is no specific reason why `x86` is used instead of `x64`. We could consider switching to it later.
1. Make sure `LearningOpenGLProject` project is **set as start-up project**. It should be in bold character in the *Solution Explorer* window.
    * If this is not the case, simply `right+click` on the `LearningOpenGLProject` and click on `Set as StartUp Project`
1. **Build the solution**: you can do this in menu *Build->Build Solution*.
1. **Press on *Local Windows Debugger***. This will do two things:
    1. Open a command prompto window that will display the logs.
    1. Open a graphics window in which the OpenGL rendering will happen.

### Write unit tests

*Google Test* and *Google Mock* are used in this project to write unit tests. Here is a step by step guide on how to create a new unit test using these technologies.

1. Create a new test file (always a `.cpp`) in the *Test* project.
    * The file should be located in the `UnitTests/` folder, under the **same folder hierarchy as the file being tested**. 
    * Example: if `Core/Input/Keyboard.h` is being tested, put the test in `UnitTests/Core/Input/KeyboardTests.cpp`.
1. At the top, include `<gtest/gtest.h>` and `<gmock/gmock.h>`.
1. If needed, create mock classes for the dependencies of the class you want to test. 
    * The mocks should be placed in the `Mocks/` folder, under **the same folder hierarchy as file being mocked**.
1. Write your test!

#### Linking errors in your tests
Since the tests are placed in a **separate** project (named *Tests*) in the solution, it is not part of the same `dll` as the code being tested. 

Normally, this means that the *Tests* project only has access to the classes exported in the `LearningOpenGLProject` `dll`.

However, since this is a special scenario where the `Tests` project needs to have access to non-exported classes, you need to do a specific manipulation. This manipulation is taken directly from the [*Microsoft* documentation](https://docs.microsoft.com/en-us/visualstudio/test/how-to-write-unit-tests-for-cpp-dlls?view=vs-2019#objectRef). 

1. `right+click` on the `Tests` project and click on `properties`
1. Go to `linker->input`. There is a field called `Additional Dependencies`.
1. In `Additional Dependencies`, add the `.obj` file of the class you are testing. You can see there are already a few `.obj` there.
    * Note: you will have to add the `.obj` of all the dependencies of the file you are testing as well.

#### Need help with *Google Test* or *Google Mock*?
Both of these framework have great documentations. Here are a few that are interesting:
##### Google Test
* [Basics (Google Test Primer)](https://github.com/google/googletest/blob/master/googletest/docs/primer.md)
* [Advanced](https://github.com/google/googletest/blob/master/googletest/docs/advanced.md)
##### Google Mock
* [Basics](https://github.com/google/googletest/blob/master/googlemock/docs/for_dummies.md)

## Project Conventions
### Folder Structure

Be aware that the folder structure refers to the **Visual Studio filters** and not the actual files location on the disk.

When creating new files, please follow this folder structure:
```
|---- Source Files (Default source file location In Visual Studio)
        |
        |---- Core All files that are not specific to a certain technology and are part of the domain of the application
        |          For example, Core will contain classes for a 3D Rendering, but no classes directly tied to "OpenGL"
        |          The folders inside it represent different topics of the application
        |_______
                |---- Rendering
                |---- Input
                |---- etc.
        |---- ExternalTechnologies  When external technologies are used or developed for certain topics
        |                           They should be placed in a topic folder inside of this one
        |                           The topic folders should reuse the same naming as used inside the "Core" folder
        |_______
                |---- Rendering
                        |---- OpenGL
                |---- Input
                        |---- GLFW
                |---- etc.

```

### Using Git
Here are a few conventions on using Git in this project.
#### Use Branches
**Never** push code directly on `master`, instead:

1. Create a new `branch`, for example: `feature/translate-player-camera`
    * `branch` should have a **descriptive name**: start it with the type of change it is (i.e. `feature`, `bug-fix`) then add a `/` followed by a short description of the change. (i.e. `translate-player-camera`).
    * Text should be all **lower-case**, separated by `-`.
1. Once all your changes are done, `rebase` your branch on `master`. This will allow you to `fast-forward merge` later and keep the history of the project clean.
1. Create a `pull request` and ask for a **code review**.
1. Once approved, `fast-forward merge` on `master`. 

#### Other Good Practices
* Do small `commits` that make sense on their own.
* The `commit` message **should start with a short sentence** that summarises the change (**max 50 characters**). Details can be added on a separate line after.
    * This will make sure that you can easily read the changes, even when viewing them from a `graph` or as a `history`.

### C++ Coding Standards
Want to know which syntax to use or what the good practices are when coding in this project?

There is a **code file** in the project that sets the standards on how things should be coded.

Look for [`CodingStandards.h`](https://bitbucket.org/et_bp/learning-opengl/master/LearningOpenGLProject/CodingStandards.h).

This file is meant to change as the project evolves. Suggestions can be made on it by making changes to it and proposing them in a `pull request`.

## Which dependencies are used in this project?

Dependency                                                                  | Version | What is it?
--------------------------------------------------------------------------- | ------- | ----------------  
[GLEW](http://glew.sourceforge.net/)                                        | 2.1.0   | _GLEW_ is an _extension wrangler_ for _OpenGL_. It provides an easy way of knowing which _OpenGL_ extensions are enabled or not on a given system.                                                                                   
[GLFW](https://www.glfw.org)                                                | 3.3.2   | _GLFW_ is a library that offers quite a few features on top of _OpenGL_ such as: creating and handling windows, handling user input, providing a time step for ticking, etc. It offers a lot of the basic features of a game engine.
[GLM](https://glm.g-truc.net/0.9.8/index.html)                              | 0.9.8.5 | _GLM_ is a C++ mathematics library for graphics programming.
[Boost](https://www.boost.org/)                                             | 1.71.0  | _Boost_ contains various well crafted C++ librairies. It is extending the standard C++ library with quite a lot of features.
[Google Test](https://github.com/google/googletest)                         | 1.8.0   | _Google Test_ is a C++ testing framework written by Google technology team. In this project, it is used to write unit tests.
[Google Mock](https://github.com/google/googletest/tree/master/googlemock)  | 1.10.0  | _Google Mock_ is a Mocking framework developed by Google. It is used in the context of unit testing to create mock classes easily.
[SOIL](http://www.lonesock.net/soil.html)                                   | 1.16.0  | _SOIL_ (Simple OpenGL Image Loader) is a small library to load images from disk in memory. It also provides OpenGL specific functionnalities like loading an image directly in an OpenGL texture.
[Assimp](http://www.assimp.org/)                                            | 3.0.0   | From the official website: "Assimp is a library to load and process geometric scenes from various data formats." In this project, it is used to import 3D scenes.
