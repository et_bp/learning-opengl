#include "ShaderUniformVariableCollectionBuilder.h"
#include "ShaderUniformVariableCollection.h"



ShaderUniformVariableCollection ShaderUniformVariableCollectionBuilder::Build()
{
	return ShaderUniformVariableCollection(BuildVariables(mFloatVariables), BuildVariables(mMatrices4Variables), BuildVariables(mVector3Variables));
}
