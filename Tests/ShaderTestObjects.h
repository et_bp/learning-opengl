#pragma once
#include "PhongLightingUniformVariables.h"
#include "Shader.h"
#include "ShaderUniformVariableCollectionBuilder.h"
#include "CameraUniformVariables.h"

namespace ShaderTestObjects
{
	/**
	 * Create an empty vertex shader source code for test purposes.
	 * It doesn't need to compile.
	 */
	inline std::string CreateVertexShaderSourceCode()
	{
		return 
			"#version 330						"
			"void main()						"
			"{									"
			"	gl_Position = vec4(0.f);		"
			"}									";
	}

	/**
	 * Create an empty fragment shader source code for test purposes.
	 * It doesn't need to compile.
	 */
	inline std::string CreateFragmentShaderSourceCode()
	{
		return
			"#version 330						"
			"void main()						"
			"{									"
			"	color = vec4(0.f);				"
			"}									";
	}

	inline PhongLightingUniformVariables CreatePhongUniformVariables()
	{
		return PhongLightingUniformVariables(
			aFloat().Named("TestAmbientLightIntensity").WithValue(1.f).Build(),
			aVector3().Named("TestAmbientLightColor").WithValue(glm::vec3(0.f, 0.f, 0.f)).Build(),
			aFloat().Named("TestDirectionalLightIntensity").WithValue(1.f).Build(),
			aVector3().Named("TestDirectionalLightColor").WithValue(glm::vec3(1.f, 1.f, 1.f)).Build(),
			aVector3().Named("TestDirectionalLightDirection").WithValue(glm::vec3(0.f, -1.f, 0.f)).Build(),
			aFloat().Named("TestMaterialSpecularIntensity").WithValue(0.3f).Build(),
			aFloat().Named("TestMaterialSpecularPrecision").WithValue(1.f).Build()
		);
	}

	inline CameraUniformVariables CreateCameraUniformVariables()
	{
		return CameraUniformVariables(
			{"TestView", glm::mat4x4()},
			{"TestModel", glm::mat4x4()},
			{"TestProjection", glm::mat4x4()},
			{"TestCameraPosition", glm::vec3(0.f, 0.f, 0.f)}
		);
	}
};

