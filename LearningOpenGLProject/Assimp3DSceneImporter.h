#pragma once
#include "I3DSceneImporter.h"

#include <glm/vec3.hpp>
#include <vector>

#include <assimp/scene.h>
#include <assimp/vector3.h>
#include <assimp/mesh.h>

struct Texel;

class Assimp3DSceneImporter : public I3DSceneImporter
{
public:

	Scene3D ImportFromFile(const std::string& iFilePath) const override;

private:

	Scene3D CreateSceneFromAssimpScene(const aiScene* iAssimpScene) const;
	void CopyAssimpNodeContentToScene(aiNode* iAssimpNode, const aiScene* iAssimpSceneWithAllData, Scene3D& iCopyTo) const;
	void CopyAssimpMeshToScene(aiMesh* iAssimpMesh, Scene3D& iCopyTo) const;

	std::vector<unsigned int> GetIndices(aiMesh* iFromAssimpMesh) const;
	std::vector<glm::vec3> GetVertices(aiMesh* iFromAssimpMesh) const;
	std::vector<glm::vec3> GetNormals(aiMesh* iFromAssimpMesh) const;
	std::vector<Texel> GetTextureMapping(aiMesh* iFromAssimpMesh) const;

	glm::vec3 ConvertVector3(const aiVector3D& iFromAssimpVector3) const;
};

