#include "ShaderBuilder.h"



Shader ShaderBuilder::Build()
{
	return Shader(mType, mSourceCode, mVariables.Build());
}
