#pragma once

#include <ostream>
#include <GL/glew.h>

/**
 * Informations about a texture created in OpenGL
 */
class OpenGLTexture
{
public:
    OpenGLTexture() = default;
    OpenGLTexture(GLuint iID)
        : mID(iID)
    {}

    operator bool() const 
    {
        return mID != 0;
    }

    bool operator==(const OpenGLTexture& iOther) const
    {
        return mID == iOther.mID;
    }

	friend std::ostream& operator<<(std::ostream& iOutStream, const OpenGLTexture& iTexture)
	{
		iOutStream << "OpenGLTexture (ID = " << iTexture.mID << ")";
		return iOutStream;
	}

    GLuint GetID() const { return mID; }

private:
    GLuint mID = 0;
};

