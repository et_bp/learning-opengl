#pragma once
#include <string>
#include <ostream>
#include <glm/gtx/string_cast.hpp>
#include <GL/glew.h>

#include "PrintExternalClasses.h"

class OpenGLUniformVariable
{
public:

	OpenGLUniformVariable(const std::string& iName) : mName(iName) {}

	virtual ~OpenGLUniformVariable() = default;

	const std::string& GetName() const { return mName; }
	GLuint GetLocation() const { return mLocation; }

	void SetLocation(GLuint iLocation) { mLocation = iLocation; }

protected:

	GLuint mLocation = GL_INVALID_INDEX;
	std::string mName;
};


template<typename T>
class OpenGLUniformVariableTyped : public OpenGLUniformVariable
{
public:

	OpenGLUniformVariableTyped(const std::string& iName, T iValue)
		: OpenGLUniformVariable(iName)
		, mValue(iValue)
	{

	}

	virtual ~OpenGLUniformVariableTyped() override = default;
	
	void SetValue(const T& iNewValue) { mValue = iNewValue; }
	const T& GetValue() const { return mValue; }

	bool operator==(const OpenGLUniformVariableTyped<T>& iOther) const;

	friend std::ostream& operator<<(std::ostream& iOutStream, const OpenGLUniformVariableTyped<T>& iVariable)
	{
		// Don't print the value because the print can become unreadable for matrices and types with a lot of data in them.
		iOutStream << "\"" << iVariable.GetName() << "\"";
		return iOutStream;
	}

private:

	T mValue;	
};

template<typename T>
bool OpenGLUniformVariableTyped<T>::operator==(const OpenGLUniformVariableTyped<T>& iOther) const
{
	return mValue == iOther.mValue && mName == iOther.mName;
}

