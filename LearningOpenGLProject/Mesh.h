#pragma once
#include <boost/optional.hpp>
#include <memory>
#include <vector>
#include <glm/vec3.hpp>

#include "Texel.h"
#include "Image.h"
#include "Material.h"

struct Image;

/**
 * 3D object defined by a list of vertices, indices and a texture mapping. It also defines the visual aspect of it's surface through a material.
 * The indices are indices of vertices. This allows the mesh to resuse certain vertices to create the 3D object.
 * The texture mapping defines which parts of the texture will be applied on which parts of the mesh.
 */
class Mesh
{
public:
	Mesh(const std::vector<unsigned int>& iIndices, const std::vector<glm::vec3>& iVertices, const std::vector<glm::vec3>& iNormals, const std::vector<Texel>& iTextureMapping)
		: mIndices(iIndices)
		, mVertices(iVertices)
		, mNormals(iNormals)
		, mTextureMapping(iTextureMapping)
	{
	}

	const std::vector<unsigned int>& GetIndices() const { return mIndices; }
	const std::vector<glm::vec3>& GetVertices() const { return mVertices; }
	const std::vector<glm::vec3>& GetNormals() const { return mNormals; }
	const std::vector<Texel>& GetTextureMapping() const { return mTextureMapping; }

	const Material& GetMaterial() const { return mMaterial; }
	void SetMaterial(const Material& iNewMaterial) { mMaterial = iNewMaterial; }

	bool operator==(const Mesh& iOther) const
	{
		return
			mIndices == iOther.mIndices &&
			mVertices == iOther.mVertices &&
			mNormals == iOther.mNormals &&
			mTextureMapping == iOther.mTextureMapping &&
			mMaterial == iOther.mMaterial;
	}

private:
	std::vector<unsigned int> mIndices;
	std::vector<glm::vec3> mVertices;
	// Normals on each vertices. Should be the same size as the mVertices.
	std::vector<glm::vec3> mNormals;

	/**
	 * The visual properties of this mesh's surface
	 */
	Material mMaterial;

	/**
	 * Which texels should be applied on which vertex.
	 * There should be as many texels as there are vertices in the mesh.
	 */
	std::vector<Texel> mTextureMapping;
};

