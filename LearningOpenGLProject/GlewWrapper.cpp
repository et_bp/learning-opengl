#include "GlewWrapper.h"

#include <stdexcept>
#include <GL/glew.h>

#include "ILogWriter.h"



GlewWrapper::GlewWrapper(ILogWriter* iLog)
	: mLog(iLog)
{
}

void GlewWrapper::Initialize()
{
	// Allow modern extension features
	glewExperimental = GL_TRUE;

	if (glewInit() != GLEW_OK)
	{
		throw std::runtime_error("Failed to initialize GLEW.");
	}

	mLog->Write("Initialized GLEW.");
}
