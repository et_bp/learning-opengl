#pragma once

#include "IOpenGLExtensionsHandler.h"

class ILogWriter;

class GlewWrapper : public IOpenGLExtensionsHandler
{
public:

	GlewWrapper(ILogWriter* iLog);

	// IOpenGLExtensionsHandler overrides begin
	void Initialize() override;
	// IOpenGLExtensionsHandler overrides end

private:

	ILogWriter* mLog;
};

