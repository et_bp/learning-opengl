#pragma once
#include <string>
#include <glm/glm.hpp>

struct ColorRGBA;
struct Dimensions2D;
struct Shader;
class Mesh;
struct Image;
struct MeshUniqueIdInRenderer;

class MeshIdNotFoundError : public std::runtime_error
{
public:
	MeshIdNotFoundError(const std::string& iWhat)
		: std::runtime_error(iWhat)
	{}
};

class IRenderer3D
{
public:
	virtual ~IRenderer3D() = default;

	/**
	 * Initialize the renderer and its viewport to a certain dimension.
	 */
    virtual void Initialise(const Dimensions2D& iViewportDimensions) = 0;

	virtual void SetBackgroundColor(const ColorRGBA& iColor) = 0;

	/**
	 * Draw the specified mesh that has been previously created in the renderer
	 * Throws MeshIdNotFoundError if the mesh was not previously created using CreateMesh
	 */
	virtual void DrawMesh(const MeshUniqueIdInRenderer& iToDraw) = 0;

	/**
	 * Create the mesh inside the renderer a unique ID that will allow you to reference the mesh created for later calls.
	 * If the mesh contains a texture, the texture will also get created inside the renderer
	 * Throws std::invalid_argument exception if the data contained in the mesh is problematic and can't be created 
	 */
	virtual MeshUniqueIdInRenderer CreateMesh(const Mesh& iToAdd) = 0;

	virtual bool IsMeshCreated(const MeshUniqueIdInRenderer& iMeshId) const = 0;

	/**
	 * Add a shader to the renderer. Call this before calling AllShadersAreSet.
	 */
	virtual void SetShader(const Shader& iShader) = 0;

	/** Tell the renderer that all shaders have been set. */
	virtual void AllShadersAreSet() = 0;

	/**
	 * Update the value in a uniform variable that is part of a shader.
	 * This can be used to change values of shaders in the viewport that is getting rendered.
	 */
	virtual void SetValueInUniformVariable(const std::string& iVariableName, float iNewValue) = 0;
	virtual void SetValueInUniformVariable(const std::string& iVariableName, const glm::mat4& iNewValue) = 0;
	virtual void SetValueInUniformVariable(const std::string& iVariableName, const glm::vec3& iNewValue) = 0;

};