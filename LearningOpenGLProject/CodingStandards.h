
// [always.pragma.once] Make sure to always put pragma once at the top of your header files
#pragma once

// [includes.external-library.brackets] All includes from external libraries should be in <>
// [includes.external-library.order] Includes from external libraries are ordered first
#include <string>
#include <vector>

// [includes.forward-declaration.always] Always prefer forward declaration if you can avoid includes

// [interface.name.prefix] Always prefix abstract interface classes with a I
class IAbstractInterfaceExample
{
public:
	virtual void MethodToOverride() = 0;
};

// [class.name.capital] Classes names start with a capital letter
// [class.name.non-generic] Avoid as much as possible generic words in your name such as "data", "info"
class ClassCodingStandards : public IAbstractInterfaceExample
{
	// [class.methods.capital] Methods names start with a capital letter.
	void StartsWithCapitalLetter() {}

	// [class.methods.parameters.prefix]	
	// Methods parameters are prefixed with 'i', 'o' or 'io'.
	// 'i' means "input parameter". It is provided to the method and is not modified. It will in general be a "const &".
	// 'o' means "output parameter". It is empty or in a "null state" when it is passed to method. It gets modified in the method.
	// 'io' means "in-out parameter". It already contains something when it is passed to the method and is modified in the method.
	void ExampleWithInputs(int iValueA, const std::string& iDescription) {}
	void ExampleWithOutputs(int ValueA, std::string& oErrorDescription) {}
	void ExampleWithInOut(std::vector<int>& ioToComplete) {}

	// [class.methods.implementation.cpp] Unless it is a trivial method or a template method, always move the implementation to the .cpp file.
	void MethodWithImplementationInCppFile();

// [class.accessibility.order]	
// Always put public, protected and private in this order: public then protected then private.
// Always having the same order means its quicker for everyone to find what they are searching for
public:

	// [class.methods.inherited.surround-comments] Always surround inherited methods with "BaseClass overrides begin" and "end"
	// IAbstractInterfaceExample overrides begin

	// [class.methods.inherited.override] Always put override in the declaration of an inherited method
	void MethodToOverride() override {}

	// IAbstractInterfaceExample overrides end

	// [class.methods.public.documentation] 
	// Unless a method is trivial (i.e. getters and setters), put documentation on your public methods
	// Use javadoc style documentation.
	/**
	 * Example to show how to format methods documentation
	 * @param iParameter1 Explanation of what the first parameter does. What kind of values are expected? What is its role in the method?
	 * @param iParameter2 Same as iParameter1, but for iParameter2.
	 * @returns When does the method return what value?
	 */
	bool HasDocumentation(int iParameter1, float iParameter2) { return true; }

protected:
private:

	// [class.member-variables.private.prefix] Prefix all private member variables with "m"
	// [class.member-variables.initialization] if the variable is a built-in type, initialize it on declaration.
	bool mPrivateMember = false;
};

// [struct.name.same-as-class] Use the same rules as a class to name a struct
struct ExampleStruct
{
	// [struct.member-variable.public.capital] Public member variable start with a capital letter. They look a lot like public methods.
	int PublicMemberVariable = 0;
};