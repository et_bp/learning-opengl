#pragma once
#include <functional>

#include "Keyboard.h"

class IKeyboardInputListener
{
public:

	/**
	 * Is the specified key currently pressed?
	 */
	virtual bool IsPressed(Keyboard::Key iWhichKey) const = 0;

	using KeyEventCallback = std::function<void()>;
	/**
	 * Bind a callback to a certain action on a certain key.
	 * @param iSomethingHappens Which action needs to happen to execute the callback
	 * @param iOnAKey On which key the action needs to happen to execute the callback
	 * @param iWhatToDo The callback that should be executed when the action is executed on the key
	 */
	virtual void When(Keyboard::Action iSomethingHappens, Keyboard::Key iOnAKey, KeyEventCallback iWhatToDo) = 0;
};
