#pragma once

#include <string>
#include <stdexcept>

#include "Image.h"

class ImageImportError : public std::runtime_error
{
public:
    ImageImportError(std::string iErrorMessage)
        : std::runtime_error(iErrorMessage)
    {}
};

/**
 * Imports images into the memory of the application
 */
class IImageImporter
{
public:

    /**
     * Import an image to an array of bytes from a location on disk relative to the solution file.
     * The path should contain both the file name and it's extension.
     * 
     * Throws a ImageImportError if there is an error when trying to import the image.
     */
    virtual std::shared_ptr<Image> ImportFromDisk(std::string iPathOnDisk) = 0;
};

