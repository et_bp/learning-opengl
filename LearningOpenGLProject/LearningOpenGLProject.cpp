
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <boost/format.hpp>

#include "GlfwWindowHandler.h"
#include "OpenGLRenderer.h"
#include "OpenGLWrapper.h"
#include "GlewWrapper.h"
#include "IOpenGLLibrary.h"
#include "IOpenGLExtensionsHandler.h"
#include "Shader.h"
#include "ShaderBuilder.h"
#include "Mesh.h"
#include "MeshUniqueIdInRenderer.h"

#include "ColorRGBA.h"
#include "Dimensions2D.h"

#include "IFileReader.h"
#include "StdFileReader.h"

#include "IKeyboardInputListener.h"
#include "GlfwInputListener.h"
#include "IMouseInputListener.h"
#include "MouseCursorMovement.h"

#include "PlayerCamera.h"
#include "FlyingPlayerCameraController.h"
#include "AircraftRotation.h"

#include "IRealTimeTicker.h"
#include "GlfwTimeWrapper.h"

#include "StdOutputLogger.h"
#include "ILogWriter.h"
#include "SOILImageImporter.h"
#include "AmbientLight.h"
#include "DirectionalLight.h"

#include "Assimp3DSceneImporter.h"
#include "Scene3D.h"
#include "PhongWorldRenderer.h"
#include "CameraUniformVariables.h"

AmbientLight mAmbientLight = AmbientLight(0.3f, { 1.f, 1.f, 1.f });
DirectionalLight mDirectionalLight = DirectionalLight(1.f, { 1.f, 1.f, 1.f }, { 0.0f, 0.0f, -1.0f });

glm::mat4 CreatePerspectiveProjection(const Dimensions2D& iViewportDimensions)
{
	return glm::perspective(45.f, iViewportDimensions.GetAspectRatio(), 0.1f, 100.f);
}

glm::mat4 ComputeModelMatrix()
	{
	glm::mat4 modelMatrix;
	modelMatrix = glm::translate(modelMatrix, glm::vec3(0.f, 0.f, -2.5f));
	modelMatrix = glm::scale(modelMatrix, glm::vec3(10.f, 10.f, 10.f));
	return modelMatrix;
}

int main()
{
	std::unique_ptr<IFileReader> fileReader = std::make_unique<StdFileReader>();
	std::unique_ptr<ILogWriter> log = std::make_unique<StdOutputLogger>();
	std::unique_ptr<IImageImporter> imageImporter = std::make_unique<SOILImageImporter>();
	std::unique_ptr<I3DSceneImporter> importer3DScene = std::make_unique<Assimp3DSceneImporter>();

	std::shared_ptr<GlfwWindowHandler> windowHandler = std::make_shared<GlfwWindowHandler>(SoftwareVersion(3, 3), log.get());

	std::unique_ptr<IOpenGLLibrary> openGL = std::make_unique<OpenGLWrapper>();
	std::unique_ptr<IOpenGLExtensionsHandler> openGLExtensionHandler = std::make_unique<GlewWrapper>(log.get());
	std::unique_ptr<IRenderer3D> renderer3D = std::make_unique<OpenGLRenderer>(openGL.get(), openGLExtensionHandler.get(), log.get());

	std::unique_ptr<PhongWorldRenderer> phongRenderer = std::make_unique<PhongWorldRenderer>(renderer3D.get());

	std::shared_ptr<GlfwInputListener> glfwInputListener = std::make_shared<GlfwInputListener>(log.get());
	windowHandler->SetListener(glfwInputListener);

	std::shared_ptr<IKeyboardInputListener> keyboardInputListener = glfwInputListener;
	std::shared_ptr<IMouseInputListener> mouseInputListener = glfwInputListener;

	std::shared_ptr<PlayerCamera> playerCamera = std::make_shared<PlayerCamera>(glm::vec3(0.f, 0.f, 0.f), AircraftRotation(-90.f, 0.f, 0.f));

	std::unique_ptr<FlyingPlayerCameraController> playerCameraController = std::make_unique<FlyingPlayerCameraController>(playerCamera, keyboardInputListener, mouseInputListener);

	try 
	{
		// Create OpenGL window

		windowHandler->Initialise();
		windowHandler->CreateWindow("OpenGL 3.3 Experiment Window", 1920, 1080);

		renderer3D->Initialise(windowHandler->GetWindowViewportDimensions());

		// Shader Creation

		phongRenderer->SetShaders(
			fileReader->ReadFile("Shaders/VertexShader.hlsl"),
			fileReader->ReadFile("Shaders/FragmentShader.hlsl"),
			PhongLightingUniformVariables(
				{ "ambientLight.intensity", 1.f },
				{ "ambientLight.color", glm::vec3(1.f, 1.f, 1.f) },
				{ "directionalLight.intensity", 1.f },
				{ "directionalLight.color", glm::vec3(1.f, 1.f, 1.f) },
				{ "directionalLight.direction", glm::vec3(0.0f, 0.0f, -1.0f) },
				{ "material.specularIntensity", 0.3f },
				{ "material.specularPrecision", 8.f }),
			CameraUniformVariables(
				{"view", glm::mat4x4()},
				{"model", glm::mat4x4()},
				{"projection", glm::mat4x4()},
				{"cameraPosition", glm::vec3(0.f, 0.f, 0.f)}
			)
		);

		phongRenderer->SetProjectionMatrix(CreatePerspectiveProjection(windowHandler->GetWindowViewportDimensions()));

		// Import 3D Scene and apply texture on meshes

		std::shared_ptr<Image> defaultTexture = imageImporter->ImportFromDisk("Textures/default-texture.png");

		auto imported3DScene = importer3DScene->ImportFromFile("3DScenes/donut-v2.dae");
		// There is currently no way to know which mesh is what, so we have to guess that the first mesh will be the icing
		auto& icingMesh = imported3DScene.GetMeshes()[0]; 
		icingMesh->SetMaterial({defaultTexture, 0.7f, 128.f}); // Icing gets a very shiny material

		auto& doughMesh = imported3DScene.GetMeshes()[1]; 
		doughMesh->SetMaterial({defaultTexture, 0.0f, 2.f}); // The dough is not shiny at all

		phongRenderer->AddMeshesToWorld(std::move(imported3DScene.GetMeshes()));

		keyboardInputListener->When(Keyboard::Action::Press, Keyboard::Key::ESC, [windowHandler]() { windowHandler->CloseWindow(); });

		std::unique_ptr<IRealTimeTicker> realTimeTicker = std::make_unique<GlfwTimeWrapper>();
		realTimeTicker->WillTick(windowHandler.get());
		realTimeTicker->WillTick(playerCameraController.get());

		phongRenderer->SetAmbientLight(mAmbientLight);
		phongRenderer->SetDirectionalLight(mDirectionalLight);

		while (windowHandler->IsWindowAlive())
		{
			realTimeTicker->TickAll();

			phongRenderer->SetModelmatrix(ComputeModelMatrix());

			playerCameraController->MoveCamera();
			phongRenderer->UpdatePlayerCamera(playerCamera->GetPosition(), playerCamera->ComputeViewMatrix());

			phongRenderer->Render();

			windowHandler->ShowViewportChangesInWindow();
		}
	}
	catch (const std::runtime_error& iError)
	{
		std::cout << "Program encountered a runtime error. It will terminate. Error: '" << iError.what() << "'\n";
		return 1;
	}
}
