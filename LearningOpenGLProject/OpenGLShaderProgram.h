#pragma once
#include <vector>
#include "OpenGLShader.h"

class OpenGLShaderProgram
{
public:
	
	OpenGLShaderProgram(GLuint iID);

	/** Attach a shader to this program. All uniform variables in the shader will be also kept in this program. */
	void AttachShader(const OpenGLShader& iShaderToAttach);
	bool IsShaderAttached(const OpenGLShader& iShader) const;

	template<typename T>
	void SetValueInUniformVariable(const OpenGLUniformVariableTyped<T>& iNewValue)
	{
		mShadersUniformVariables.UpdateVariableValue(iNewValue);
	}

	GLuint GetID() const { return mID; }
	void SetID(GLuint val) { mID = val; }

	const OpenGLUniformVariablesCollection& GetUniformVariablesCollection() const { return mShadersUniformVariables; }
	OpenGLUniformVariablesCollection& GetUniformVariablesCollection() { return mShadersUniformVariables; }

	bool operator==(const OpenGLShaderProgram& iOther) const;

private:

	void AddUniformVariables(const OpenGLUniformVariablesCollection& iUniformVariables);

	GLuint mID;

	std::vector<OpenGLShader> mShadersAttached;

	/** All uniform variables in the attached shaders */
	OpenGLUniformVariablesCollection mShadersUniformVariables;
};

inline std::ostream& operator<<(std::ostream& iStream, const OpenGLShaderProgram& iToStream)
{
	iStream << "Shader Program (ID=" << iToStream.GetID() << ")";
	return iStream;
}