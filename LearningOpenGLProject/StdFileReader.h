#pragma once
#include "IFileReader.h"

/**
 * Read file located on the disk using the std library.
 */
class StdFileReader : public IFileReader
{
public:
	// IFileReader overrides begin

	std::string ReadFile(const std::string& iPathRelativeToSolution) override;

	// IFileReader overrides end
};

