#version 330

in vec2 fragmentShader_textureCoordinate;
in vec3 fragmentShader_normal;
in vec3 fragmentShader_position;

out vec4 color;

struct AmbientLight
{
	float intensity;
	vec3 color;
};

struct DirectionalLight
{
	float intensity;
	vec3 color;
	vec3 direction;
};

struct Material
{
	// How much light the specular reflection will add to the surface
	// Should be a value between 0 and 1. 0 means it will add nothing, 1 means it will add a lot of light.
	float specularIntensity;

	// How spread the specular reflection will be. Should be a value that is a power of 2.
	// The higher the value, the more the reflection will show only around the perfect reflection to the camera
	// Perfect reflection means that the incoming light bounces directly to the camera
	float specularPrecision;
};

vec4 computeAmbientLightColorFactor(const in AmbientLight ambientLight)
{
	return vec4(ambientLight.color, 1.0f) * ambientLight.intensity;
}

vec4 computeDirectionalLightColorFactor(const in DirectionalLight directionalLight, const in vec3 normal, out bool hasAnyLight)
{
	// dot(normal, Direction) will give a cosine with the angle between these two vectors, which is conveniently a value between [-1, 1].
	// However, for a color value, we want this to be between [0,1], which is why the max() is needed.
	float colorFactor = max(dot(normalize(normal), normalize(directionalLight.direction)), 0.f);
	if (colorFactor > 0.0f)
	{
		hasAnyLight = true;
		return vec4(directionalLight.color, 1.f) * directionalLight.intensity * colorFactor;
	}
	else
	{
		hasAnyLight = false;
		return vec4(0.f, 0.f, 0.f, 0.f);
	}
}

vec3 computeReflection(const in vec3 normal, const in vec3 incomingLight)
{
	return normalize(reflect(normalize(incomingLight), normalize(normal)));
}

vec4 computeSpecularLightColorFactor(
	const in vec3 reflectedLightDirection, 
	const in vec3 reflectionPointToCamera, 
	const in Material materialReceivingLight, 
	const in vec4 lightColor)
{
	float specularFactor = dot(reflectedLightDirection, normalize(reflectionPointToCamera));
	if (specularFactor > 0.f)
	{
		float specularFactor = pow(specularFactor, materialReceivingLight.specularPrecision);
		return vec4(lightColor * materialReceivingLight.specularIntensity * specularFactor);
	}

	return vec4(0.f, 0.f, 0.f, 0.f);
}

uniform sampler2D textureSamplerForUnit0;
uniform AmbientLight ambientLight;
uniform DirectionalLight directionalLight;
uniform Material material;
uniform vec3 cameraPosition;

void main()
{
	bool anyDirectionalLightOnFragment = false;
	vec4 directionalLightColorFactor = computeDirectionalLightColorFactor(directionalLight, fragmentShader_normal, anyDirectionalLightOnFragment);
	
	vec4 specularColorFactor = vec4(0.f, 0.f, 0.f, 0.f);
	if (anyDirectionalLightOnFragment)
	{
		vec3 reflectionDirection = computeReflection(fragmentShader_normal, directionalLight.direction);
		vec3 fragmentToCamera = cameraPosition - fragmentShader_position;
		specularColorFactor = computeSpecularLightColorFactor(reflectionDirection, fragmentToCamera, material, vec4(directionalLight.color, 0.f));
	}

	color = texture(textureSamplerForUnit0, fragmentShader_textureCoordinate) * (computeAmbientLightColorFactor(ambientLight) + directionalLightColorFactor + specularColorFactor);
}