#pragma once

#include <string>

class ILogWriter
{
public:
	virtual void Write(const std::string& iText) = 0;
};

