#pragma once

#include <iterator>
#include <ostream>
#include <vector>
#include <glm/gtx/string_cast.hpp>

/************************************************************************/
/* Library std                                                          */
/************************************************************************/

template<typename T>
inline std::ostream& operator<<(std::ostream& iOutStream, const std::vector<T>&  iVector)
{
	iOutStream << "{ ";
	if (!iVector.empty())
	{
		for (std::vector<T>::const_iterator it = iVector.begin(); it != iVector.end(); ++it)
		{
			iOutStream << *it;
			if (std::next(it) != iVector.end())
			{
				iOutStream << ", ";
			}
		}
	}
	iOutStream << " }";
	return iOutStream;
}

/************************************************************************/
/* Library glm                                                          */
/************************************************************************/

inline std::ostream& operator<<(std::ostream& iOutStream, const glm::mat4& iMatrix)
{
	iOutStream << glm::to_string(iMatrix);
	return iOutStream;
}
