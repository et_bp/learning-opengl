#pragma once

#include <string>

struct Dimensions2D;

/**
 * Creates and manages a window
 */
class IWindowHandler
{
public:
    IWindowHandler() {}
	virtual ~IWindowHandler() = default;

    virtual void Initialise() = 0;

    virtual void CreateWindow(std::string iName, unsigned int iPixelWidth, unsigned int iPixelHeight) = 0;
	virtual void CloseWindow() = 0;

	virtual Dimensions2D GetWindowViewportDimensions() const = 0;
	virtual bool IsWindowAlive() const = 0;

	virtual void ShowViewportChangesInWindow() = 0;
};