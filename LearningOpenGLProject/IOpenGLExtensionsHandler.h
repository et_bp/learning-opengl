#pragma once

/**
 * Tells if a certain OpenGL extension is available or not on the machine
 */
class IOpenGLExtensionsHandler
{
public:
	virtual void Initialize() = 0;
};

