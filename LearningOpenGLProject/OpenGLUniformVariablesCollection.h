#pragma once
#include <memory>
#include <vector>
#include <ostream>
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "OpenGLUniformVariable.h"
#include "PrintExternalClasses.h"

class OpenGLUniformVariablesCollection
{
public:
	OpenGLUniformVariablesCollection() = default;
	OpenGLUniformVariablesCollection(const std::vector<OpenGLUniformVariableTyped<GLfloat>>& iFloats);
	
	void Add(const OpenGLUniformVariablesCollection& iVariablesToAdd);

	template<typename T>
	void Add(const std::vector<OpenGLUniformVariableTyped<T>>& iToAdd)
	{
		auto& ourVariables = GetVariables<T>();
		ourVariables.insert(ourVariables.end(), iToAdd.begin(), iToAdd.end());
	}

	template<typename T>
	void Add(const OpenGLUniformVariableTyped<T>& iToAdd)
	{
		GetVariables<T>().push_back(iToAdd);
	}

	template<typename T>
	void UpdateVariableValue(const OpenGLUniformVariableTyped<T>& iNewValue)
	{
		for (auto& ourVariable : GetVariables<T>())
		{
			if (ourVariable.GetName() == iNewValue.GetName())
			{
				ourVariable.SetValue(iNewValue.GetValue());
			}
		}
	}

	std::vector<OpenGLUniformVariable*> GetAll();

	// Const version of the GetVariables
	template<typename T>
	const std::vector<OpenGLUniformVariableTyped<T>>& GetVariables() const { return const_cast<OpenGLUniformVariablesCollection*>(this)->GetVariables<T>(); }

	template<typename T>
	std::vector<OpenGLUniformVariableTyped<T>>& GetVariables() { BOOST_STATIC_ASSERT_MSG(false, "Uniform variable type not yet supported. Please add support for it."); }
	template<>
	std::vector<OpenGLUniformVariableTyped<float>>& GetVariables() { return mFloats; }
	template<>
	std::vector<OpenGLUniformVariableTyped<glm::mat4>>& GetVariables() { return mMatrices4; }
	template<>
	std::vector<OpenGLUniformVariableTyped<glm::vec3>>& GetVariables() { return mVectors3; }

	bool operator==(const OpenGLUniformVariablesCollection& iOther) const;

	friend std::ostream& operator<<(std::ostream& iOutStream, const OpenGLUniformVariablesCollection& iVariablesCollection);

private:

	std::vector<OpenGLUniformVariableTyped<GLfloat>> mFloats;
	std::vector<OpenGLUniformVariableTyped<glm::mat4>> mMatrices4;
	std::vector<OpenGLUniformVariableTyped<glm::vec3>> mVectors3;
};
