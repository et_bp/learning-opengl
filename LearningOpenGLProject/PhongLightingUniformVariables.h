#pragma once

#include <glm/vec3.hpp>
#include <string>

#include "ShaderUniformVariable.h"
#include "Shader.h"
#include "ShaderUniformVariableCollection.h"


/**
 * Uniform variables that are required the phong lighting model in a shader
 */
class PhongLightingUniformVariables
{
public:

	PhongLightingUniformVariables(
		const ShaderUniformVariable<float>& iAmbientLightIntensityVariable,
		const ShaderUniformVariable<glm::vec3>& iAmbientLightColorVariable,
		const ShaderUniformVariable<float>& iDirectionalLightIntensityVariable,
		const ShaderUniformVariable<glm::vec3>& iDirectionalLightColorVariable,
		const ShaderUniformVariable<glm::vec3>& iDirectionalLightDirectionVariable,
		const ShaderUniformVariable<float> iMaterialSpecularIntensityVariable,
		const ShaderUniformVariable<float> iMaterialSpecularPrecisionVariable
	);

	/**
	 * Get all uniform variables to add to a shader of the given type
	 * Will return an empty collection if the are no variables for a certain shader type
	 */
	ShaderUniformVariableCollection GetAllVariablesForShader(Shader::Type iForType) const;

	std::string GetAmbientLightIntensityVariableName() const { return mAmbientLightIntensity.GetName(); }
	std::string GetAmbientLightColorVariableName() const { return mAmbientLightColor.GetName(); }
	
	std::string GetDirectionalLightIntensityVariableName() const { return mDirectionalLightIntensity.GetName(); }
	std::string GetDirectionalLightColorVariableName() const { return mDirectionalLightColor.GetName(); }
	std::string GetDirectionalLightDirectionVariableName() const { return mDirectionalLightDirection.GetName(); }

	std::string GetMaterialSpecularIntensityVariableName() const { return mMaterialSpecularIntensity.GetName(); }
	std::string GetMaterialSpecularPrecisionVariableName() const { return mMaterialSpecularPrecision.GetName(); }

private:

	ShaderUniformVariable<float> mAmbientLightIntensity;
	ShaderUniformVariable<glm::vec3> mAmbientLightColor;

	ShaderUniformVariable<float> mDirectionalLightIntensity;
	ShaderUniformVariable<glm::vec3> mDirectionalLightColor;
	ShaderUniformVariable<glm::vec3> mDirectionalLightDirection;

	ShaderUniformVariable<float> mMaterialSpecularIntensity;
	ShaderUniformVariable<float> mMaterialSpecularPrecision;
};

