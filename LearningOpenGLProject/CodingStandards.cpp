
// [include.cpp.header-first] Always include the header linked to the .cpp file first
#include "CodingStandards.h"

void ClassCodingStandards::MethodWithImplementationInCppFile()
{
	// Implementation that is not trivial, so it is placed in the ".cpp" file
}

void FunctionWithCodeSamples()
{
	// [namespace.specify] 
	// Always put the namespace explicitly before using something in another namespace. For example, put "std" before string.
	// This also means that we don't use "using namespace ..."
	// The main reason for this is to make sure the developer has a good understanding of where a type or function comes from. He has a good visibility on the dependencies.
	std::string aStringWithNamespace = "std::Hello? No!";
}
