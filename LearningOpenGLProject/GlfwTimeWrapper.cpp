#include "GlfwTimeWrapper.h"

#include <GLFW/glfw3.h>

#include "IRunsInRealTime.h"



void GlfwTimeWrapper::WillTick(IRunsInRealTime* iToTick)
{
	mObjectsToTick.push_back(iToTick);
}

void GlfwTimeWrapper::TickAll()
{
	float deltaTimeSinceLastTick = GetDeltaTime();
	for (IRunsInRealTime* toTick : mObjectsToTick)
	{
		toTick->Tick(deltaTimeSinceLastTick);
	}
}

float GlfwTimeWrapper::GetDeltaTime()
{
	float now = (float)glfwGetTime();
	float deltaTime = now - mLastTickTime;
	mLastTickTime = now;
	return deltaTime;
}