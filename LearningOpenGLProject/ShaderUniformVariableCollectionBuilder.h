#pragma once

#include <vector>
#include <glm/glm.hpp>

#include "ShaderUniformVariableBuilder.h"

class ShaderUniformVariableCollection;


class ShaderUniformVariableCollectionBuilder
{
public:

	ShaderUniformVariableCollection Build();

	ShaderUniformVariableCollectionBuilder& With(const ShaderUniformVariableBuilder<float>& iFloatUniformVar)
	{
		mFloatVariables.push_back(iFloatUniformVar);
		return *this;
	}

	ShaderUniformVariableCollectionBuilder& With(const ShaderUniformVariableBuilder<glm::mat4>& iMatrix4UniformVar)
	{
		mMatrices4Variables.push_back(iMatrix4UniformVar);
		return *this;
	}

	ShaderUniformVariableCollectionBuilder& With(const ShaderUniformVariableBuilder<glm::vec3>& iVec3UniformVar)
	{
		mVector3Variables.push_back(iVec3UniformVar);
		return *this;
	}

private:

	std::vector<ShaderUniformVariableBuilder<float>> mFloatVariables;
	std::vector<ShaderUniformVariableBuilder<glm::mat4>> mMatrices4Variables;
	std::vector<ShaderUniformVariableBuilder<glm::vec3>> mVector3Variables;

	template<typename T>
	std::vector<ShaderUniformVariable<T>> BuildVariables(const std::vector<ShaderUniformVariableBuilder<T>>& iFromBuilders)
	{
		std::vector<ShaderUniformVariable<T>> builtVariables;
		for each (const ShaderUniformVariableBuilder<T>& variableBuilder in iFromBuilders)
		{
			builtVariables.push_back(variableBuilder.Build());
		}

		return builtVariables;
	}
};

inline ShaderUniformVariableCollectionBuilder UniformVariables() { return ShaderUniformVariableCollectionBuilder(); }

inline ShaderUniformVariableBuilder<float> aFloat() { return ShaderUniformVariableBuilder<float>(); }
inline ShaderUniformVariableBuilder<glm::mat4> aMatrix4() { return ShaderUniformVariableBuilder<glm::mat4>(); }
inline ShaderUniformVariableBuilder<glm::vec3> aVector3() { return ShaderUniformVariableBuilder<glm::vec3>(); }