#include "MouseCursorPosition.h"

#include <boost/format.hpp>

std::string MouseCursorPosition::ToString()
{
	return (boost::format("(%1%, %2%)") % CursorPosition.x % CursorPosition.y).str();
}
