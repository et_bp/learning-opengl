#pragma once

#include "ILogWriter.h"

class StdOutputLogger : public ILogWriter
{
	// ILogWriter overrides begin

	void Write(const std::string& iText) override;

	// ILogWriter overrides end
};

