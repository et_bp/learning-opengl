#pragma once

#include <string>
#include <stdexcept>

class Scene3D;

class Import3DSceneError : public std::runtime_error
{
public:
	Import3DSceneError(std::string iErrorMessage)
		: std::runtime_error(iErrorMessage)
	{}
};

class I3DSceneImporter
{
public:

	/**
	 * Import a 3D scene file from path on the disk.
	 * @params iFilePath Path relative to the solution of this project.
	 *
	 * Throws Import3DSceneError if import failed
	 */
	virtual Scene3D ImportFromFile(const std::string& iFilePath) const = 0;
};

