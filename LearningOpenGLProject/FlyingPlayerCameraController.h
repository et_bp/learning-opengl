#pragma once

#include <memory>
#include <glm/vec3.hpp>

#include "Keyboard.h"
#include "IRunsInRealTime.h"

class IKeyboardInputListener;
class PlayerCamera;
class IMouseInputListener;
struct AircraftRotation;
struct CameraMovement;

/**
 * Moves a player camera based on the user inputs.
 * Moves the camera like a first person flying character.
 */
class FlyingPlayerCameraController : public IRunsInRealTime
{
public:

	// Nested structs

	struct SpeedParameters
	{
		SpeedParameters() = default;
		SpeedParameters(float iMovementSpeed, float iRotationSpeed)
			: MovementSpeed(iMovementSpeed)
			, RotationSpeed(iRotationSpeed)
		{}

		float MovementSpeed = 5.f;
		float RotationSpeed = 0.25f;
	};

	struct Controls
	{
		Controls() = default;
		Controls(Keyboard::Key iForward, Keyboard::Key iBackward, Keyboard::Key iStrafeRight, Keyboard::Key iStrafeLeft)
			: MoveForwardKey(iForward)
			, MoveBackwardKey(iBackward)
			, StrafeRightKey(iStrafeRight)
			, StrafeLeftKey(iStrafeLeft)
		{}

		Keyboard::Key MoveForwardKey = Keyboard::Key::W;
		Keyboard::Key MoveBackwardKey = Keyboard::Key::S;
		Keyboard::Key StrafeRightKey = Keyboard::Key::D;
		Keyboard::Key StrafeLeftKey = Keyboard::Key::A;
	};

	// Constructors

	FlyingPlayerCameraController(std::weak_ptr<PlayerCamera> iControlledCamera, std::weak_ptr<IKeyboardInputListener> iKeyboardListener, std::weak_ptr<IMouseInputListener> iMouseListener, SpeedParameters iSpeedParameters = SpeedParameters(), Controls iControls = Controls());

	// IRunsInRealTime overrides begin

	void Tick(float iDeltaInSeconds) override;

	// IRunsInRealTime overrides end

	/**
	 * Move the camera based on the inputs and the time spent this tick
	 */
	void MoveCamera();

private:

	/**
	 * Get how much the camera should move based on the keyboard inputs and the delta time
	 */
	CameraMovement GetMovementThisTick(float iDeltaInSeconds) const;

	AircraftRotation GetRotationThisTick() const;

	// Everything that is configurable in this controller

	SpeedParameters mSpeedParameters;
	Controls mControls;

	float mTimeSpentThisTick = 0.f;

	// Dependencies
	std::weak_ptr<PlayerCamera> mControlledCamera;
	std::weak_ptr<IKeyboardInputListener> mKeyboardListener;
	std::weak_ptr<IMouseInputListener> mMouseInputListener;
};

