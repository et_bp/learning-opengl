#pragma once

/**
 * Red-Green-Blue-Alpha color value where all values are within [0.0-1.0]
 */
struct ColorRGBA
{
	static const ColorRGBA Black;

	ColorRGBA(float iRed, float iGreen, float iBlue, float iAlpha);

	float Red = 1.f;
	float Green = 1.f;
	float Blue = 1.f;
	float Alpha = 1.f;
};

