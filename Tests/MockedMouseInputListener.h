#pragma once

#include <gmock/gmock.h>

#include "../LearningOpenGLProject/IMouseInputListener.h"
#include "../LearningOpenGLProject/MouseCursorMovement.h"

class MockedMouseInputListener : public IMouseInputListener
{
public:
	MOCK_METHOD(MouseCursorMovement, GetMovementThisTick, (), (override));
};

