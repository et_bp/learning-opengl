#include "ColorRGBA.h"

const ColorRGBA ColorRGBA::Black = {0.f, 0.f, 0.f, 1.f};

ColorRGBA::ColorRGBA(float iRed, float iGreen, float iBlue, float iAlpha)
	: Red(iRed), Green(iGreen), Blue(iBlue), Alpha(iAlpha)
{
}
