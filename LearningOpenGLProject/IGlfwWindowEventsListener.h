#pragma once

struct GLFWwindow;

/**
 * Defines a class that wants to receive the events of a GLFW window
 */
class IGlfwWindowEventsListener
{
public:

	virtual void OnWindowInitialized(GLFWwindow* iWindow) = 0;
	virtual void BeforePollWindowEvents() = 0;
};

