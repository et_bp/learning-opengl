#pragma once

#include <glm/vec2.hpp>
#include <string>

struct MouseCursorPosition
{
	MouseCursorPosition() = default;
	MouseCursorPosition(double iX, double iY) : CursorPosition(iX, iY) {}
	MouseCursorPosition(const glm::vec2& iCursorPosition) : CursorPosition(iCursorPosition) {}

	std::string ToString();

	glm::vec2 CursorPosition;
};

