#include "FlyingPlayerCameraController.h"

#include <functional>
#include <iostream>

#include "IKeyboardInputListener.h"
#include "PlayerCamera.h"
#include "IMouseInputListener.h"
#include "AircraftRotation.h"
#include "MouseCursorMovement.h"
#include "CameraMovement.h"


FlyingPlayerCameraController::FlyingPlayerCameraController(std::weak_ptr<PlayerCamera> iControlledCamera, std::weak_ptr<IKeyboardInputListener> iKeyboardListener, std::weak_ptr<IMouseInputListener> iMouseListener, SpeedParameters iSpeedParameters /*= Speed()*/, Controls iControls /*= Controls()*/)
	: mControlledCamera(iControlledCamera)
	, mKeyboardListener(iKeyboardListener)
	, mMouseInputListener(iMouseListener)
	, mSpeedParameters(iSpeedParameters)
	, mControls(iControls)
{

}

void FlyingPlayerCameraController::Tick(float iDeltaInSeconds)
{
	mTimeSpentThisTick = iDeltaInSeconds;
}

void FlyingPlayerCameraController::MoveCamera()
{
	auto controlledCamera = mControlledCamera.lock();
	controlledCamera->Move(GetRotationThisTick(), GetMovementThisTick(mTimeSpentThisTick));
}

CameraMovement FlyingPlayerCameraController::GetMovementThisTick(float iDeltaInSeconds) const
{
	CameraMovement movement;
	float movementSpeedForThisTick = mSpeedParameters.MovementSpeed * iDeltaInSeconds;

	auto keyboardListener = mKeyboardListener.lock();

	if (keyboardListener->IsPressed(mControls.MoveForwardKey))
	{
		movement.Forward += movementSpeedForThisTick;
	}

	if (keyboardListener->IsPressed(mControls.MoveBackwardKey))
	{
		movement.Forward += -1.f * movementSpeedForThisTick;
	}

	if (keyboardListener->IsPressed(mControls.StrafeRightKey))
	{
		movement.Right += movementSpeedForThisTick;
	}

	if (keyboardListener->IsPressed(mControls.StrafeLeftKey))
	{
		movement.Right += -1.f * movementSpeedForThisTick;
	}

	return movement;
}

AircraftRotation FlyingPlayerCameraController::GetRotationThisTick() const
{
	AircraftRotation rotationThisTick(0.f, 0.f, 0.f);

	MouseCursorMovement mouseMovement = mMouseInputListener.lock()->GetMovementThisTick();
	glm::vec2 mouseDelta = mouseMovement.GetDelta();
	auto rotationSpeed = mSpeedParameters.RotationSpeed;

	if (glm::abs(mouseDelta.x) > 0.f)
	{
		rotationThisTick.Yaw += rotationSpeed * mouseDelta.x;
	}

	if (glm::abs(mouseDelta.y) > 0.f)
	{
		rotationThisTick.Pitch += rotationSpeed * mouseDelta.y;
	}

	return rotationThisTick;
}

