#include "CameraUniformVariables.h"

CameraUniformVariables::CameraUniformVariables(
	ShaderUniformVariable<glm::mat4x4> iView, 
	ShaderUniformVariable<glm::mat4x4> iModel, 
	ShaderUniformVariable<glm::mat4x4> iProjection, 
	ShaderUniformVariable<glm::vec3> iCameraPosition)
	: mView(iView)
	, mModel(iModel)
	, mProjection(iProjection)
	, mCameraPosition(iCameraPosition)
{
}

ShaderUniformVariableCollection CameraUniformVariables::GetAllVariablesForShader(Shader::Type iForType) const
{
	switch (iForType)
	{
	case Shader::Type::Vertex:
		return
		{
			{},
			{mView, mModel, mProjection},
			{}
		};
		break;
	case Shader::Type::Fragment:
		return
		{
			{},
			{},
			{mCameraPosition}
		};
		break;
	default:
		return {};
		break;
	}
}
