#pragma once

#include "IRenderer3D.h"
#include <memory>
#include <glm/glm.hpp>
#include <boost/optional.hpp>
#include <unordered_map>
#include <GL/glew.h>
#include <stdexcept>

#include "OpenGLShaderProgram.h"
#include "OpenGLMesh.h"
#include "MeshUniqueIdInRenderer.h"

class OpenGLMesh;
class Mesh;
class ILogWriter;
class IOpenGLExtensionsHandler;
class IOpenGLLibrary;

class OpenGLRenderer : public IRenderer3D
{
public:
	OpenGLRenderer(IOpenGLLibrary* iOpenGL, IOpenGLExtensionsHandler* iExtensionHandler , ILogWriter* iLogWriter);
	~OpenGLRenderer() override {}

	// IRenderer3D overrides begin

	void Initialise(const Dimensions2D& iViewportDimensions) override;

	void SetBackgroundColor(const ColorRGBA& iColor) override;

	void DrawMesh(const MeshUniqueIdInRenderer& iToDraw) override;

	void SetShader(const Shader& iShader) override;
	void AllShadersAreSet() override;

	MeshUniqueIdInRenderer CreateMesh(const Mesh& iToAdd) override;

	bool IsMeshCreated(const MeshUniqueIdInRenderer& iMeshId) const override;

	void SetValueInUniformVariable(const std::string& iVariableName, float iNewValue) override;
	void SetValueInUniformVariable(const std::string& iVariableName, const glm::mat4& iNewValue) override;
	void SetValueInUniformVariable(const std::string& iVariableName, const glm::vec3& iNewValue) override;

	// IRenderer3D overrides end

	const OpenGLShaderProgram* GetCurrentShaderProgram();

	/**
	 * Get the mesh created with the specified Id.
	 * Throws MeshIdNotFoundError if the iMeshId couldn't be found
	 */
	const OpenGLMesh& GetCreatedMesh(const MeshUniqueIdInRenderer& iMeshId) const;

private:

	bool IsShaderProgramInitialized();
	void UpdateUniformVariablesValues(const OpenGLShaderProgram& iProgram);

	MeshUniqueIdInRenderer GetNextMeshId();

	unsigned int mLastUsedId = 0;
	std::unordered_map<MeshUniqueIdInRenderer, OpenGLMesh, MeshUniqueIdInRendererHash> mCreatedMeshes;

	// Dependencies

	IOpenGLLibrary* mOpenGL;
	IOpenGLExtensionsHandler* mExtensionHandler;
	ILogWriter* mLog;

	boost::optional<OpenGLShaderProgram> mShaderProgram;
};


