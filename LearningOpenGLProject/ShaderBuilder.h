#pragma once

#include <string>

#include "Shader.h"
#include "ShaderUniformVariableCollectionBuilder.h"

class ShaderBuilder
{
public:

	Shader Build();

	ShaderBuilder& WithType(Shader::Type iType) { mType = iType; return *this; }
	ShaderBuilder& WithSourceCode(const std::string& iCode) { mSourceCode = iCode; return *this; }
	ShaderBuilder& WithVariables(const ShaderUniformVariableCollectionBuilder& iVariables) { mVariables = iVariables; return *this; }

private:
	Shader::Type mType;
	std::string mSourceCode;
	ShaderUniformVariableCollectionBuilder mVariables;
};

inline ShaderBuilder aVertexShader()
{
	ShaderBuilder vertexShader;
	return vertexShader.WithType(Shader::Type::Vertex);
}

inline ShaderBuilder aFragmentShader()
{
	ShaderBuilder vertexShader;
	return vertexShader.WithType(Shader::Type::Fragment);
}
