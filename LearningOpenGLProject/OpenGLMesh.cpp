#include "OpenGLMesh.h"

bool OpenGLMesh::operator==(const OpenGLMesh& iOther) const
{
	return
		mTexture == iOther.mTexture &&
		mIndices == iOther.mIndices &&
		mVertices == iOther.mVertices &&
		mNormals == iOther.mNormals &&
		mVAO == iOther.mVAO &&
		mVBO == iOther.mVBO &&
		mIBO == iOther.mIBO;
}
