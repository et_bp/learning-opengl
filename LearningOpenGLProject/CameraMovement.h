#pragma once

struct CameraMovement
{
	CameraMovement() = default;
	CameraMovement(float iForward, float iRight)
		: Forward(iForward)
		, Right(iRight)
	{}

	float Forward = 0.f;
	float Right = 0.f;
};

