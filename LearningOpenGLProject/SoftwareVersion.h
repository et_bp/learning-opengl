#pragma once

struct SoftwareVersion
{
    SoftwareVersion(unsigned int iMajorVersion, unsigned int iMinorVersion): MajorVersion(iMajorVersion), MinorVersion(iMinorVersion) {}
	unsigned int MajorVersion = 0;
	unsigned int MinorVersion = 0;
};