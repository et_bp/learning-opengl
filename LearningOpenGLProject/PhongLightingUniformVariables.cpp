#include <boost/assert.hpp>

#include "PhongLightingUniformVariables.h"
#include "Shader.h"

PhongLightingUniformVariables::PhongLightingUniformVariables(
	const ShaderUniformVariable<float>& iAmbientLightIntensityVariable, 
	const ShaderUniformVariable<glm::vec3>& iAmbientLightColor,
	const ShaderUniformVariable<float>& iDirectionalLightIntensityVariable,
	const ShaderUniformVariable<glm::vec3>& iDirectionalLightColorVariable,
	const ShaderUniformVariable<glm::vec3>& iDirectionalLightDirectionVariable,
	const ShaderUniformVariable<float> iMaterialSpecularIntensityVariable,
	const ShaderUniformVariable<float> iMaterialSpecularPrecisionVariable)
	: mAmbientLightIntensity(iAmbientLightIntensityVariable)
	, mAmbientLightColor(iAmbientLightColor)
	, mDirectionalLightIntensity(iDirectionalLightIntensityVariable)
	, mDirectionalLightDirection(iDirectionalLightDirectionVariable)
	, mDirectionalLightColor(iDirectionalLightColorVariable)
	, mMaterialSpecularIntensity(iMaterialSpecularIntensityVariable)
	, mMaterialSpecularPrecision(iMaterialSpecularPrecisionVariable)
{
}

ShaderUniformVariableCollection PhongLightingUniformVariables::GetAllVariablesForShader(Shader::Type iForType) const
{
	switch (iForType)
	{
	case Shader::Type::Vertex:
		return {};
		break;
	case Shader::Type::Fragment:
		return 
		{
			{mAmbientLightIntensity, mDirectionalLightIntensity, mMaterialSpecularIntensity, mMaterialSpecularPrecision},
			{},
			{mAmbientLightColor, mDirectionalLightColor, mDirectionalLightDirection}
		};
		break;
	default:
		return {};
		break;
	}
}
