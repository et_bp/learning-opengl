#pragma once
#include "IWorldRenderer.h"

#include <vector>
#include <string>
#include <glm/mat4x4.hpp>
#include <stdexcept>

#include "Mesh.h"
#include "MeshUniqueIdInRenderer.h"
#include "PhongLightingUniformVariables.h"
#include "CameraUniformVariables.h"

class IRenderer3D;
class AmbientLight;
class DirectionalLight;

class NoShaderSetError : std::runtime_error
{
public:
	NoShaderSetError(const std::string& iWhat)
		: std::runtime_error(iWhat)
	{}
};

class PhongWorldRenderer : public IWorldRenderer
{
public:

	PhongWorldRenderer(IRenderer3D* iRenderer);

	/**
	 * Set the shaders to use when rendering the world
	 */
	void SetShaders(const std::string& iVertexShaderCode, const std::string& iFragmentShaderCode, const PhongLightingUniformVariables& iPhongUniformVariables, const CameraUniformVariables& iCameraUniformVariables);

	/**
	 * Update the ambient light currently used in the world.
	 * The ambient light is a light without direction that gets applied to everything in the scene.
	 * It doesn't create any shadows or difference in lighting, it is the same for everything.
	 * Throws NoShaderSetError if the shaders have not been set yet, make sure to call SetShaders first
	 */
	void SetAmbientLight(const AmbientLight& iNewAmbientLight);

	/**
	 * Update the directional light currently used in the world.
	 * As it's name imply, the directional is a light with a direction.
	 * You could think of it as the sun in the world. It lights everything in the world from a certain angle.
	 * Throws NoShaderSetError if the shaders have not been set yet, make sure to call SetShaders first
	 */
	void SetDirectionalLight(const DirectionalLight& iNewDirectionalLight);

	//~Begin IWorldRenderer overrides

	void AddMeshToWorld(std::unique_ptr<Mesh>&& iMeshToRender) override;
	void AddMeshesToWorld(std::vector<std::unique_ptr<Mesh>>&& iMeshesToAdd) override;

	/**
	 * Throws NoShaderSetError if the shaders have not been set yet, make sure to call SetShaders first
	 */
	void UpdatePlayerCamera(const glm::vec3& iPosition, const glm::mat4x4& iViewMatrix) override;

	/**
	 * Set the projection matrix of the camera
	 * Throws NoShaderSetError if the shaders have not been set yet, which means the projection can't be updated
	 */
	void SetProjectionMatrix(const glm::mat4x4& iNewProjectionMatrix) override;

	/**
	 * Throws NoShaderSetError if the shaders have not been set yet, make sure to call SetShaders first
	 */
	void SetModelmatrix(const glm::mat4x4& iNewModelMatrix) override;

	void Render() override;

	//~End IWorldRenderer overrides

private:

	void SetMaterialUniformVariables(const Material& iMaterial);

	struct MeshCreatedInRenderer
	{
		MeshCreatedInRenderer(std::unique_ptr<Mesh>&& iMeshCreated, const MeshUniqueIdInRenderer& iIdInRenderer);

		std::unique_ptr<Mesh> MeshCreated;
		MeshUniqueIdInRenderer IdInRenderer;
	};

	std::vector<MeshCreatedInRenderer> mMeshesToRender;

	// Shaders

	boost::optional<PhongLightingUniformVariables> mPhongUniformVariables;
	boost::optional<CameraUniformVariables> mCameraUniformVariables;

	// 3D Renderer used to render all the meshes of the world
	IRenderer3D* mRenderer;

};

