#pragma once

/**
 * Defines a class that needs to be updated (a.k.a. ticked) in real time
 */
class IRunsInRealTime
{
public:
	virtual void Tick(float iDeltaSeconds) = 0;
};

