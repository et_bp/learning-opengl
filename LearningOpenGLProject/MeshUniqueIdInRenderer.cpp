#include "MeshUniqueIdInRenderer.h"

#include <iostream>



std::ostream& operator<<(std::ostream& ioStream, const MeshUniqueIdInRenderer& iToStream)
{
	ioStream << iToStream.IdInRenderer;
	return ioStream;
}
