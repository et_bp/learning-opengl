#include "OpenGLWrapper.h"

#include <stdexcept>
#include <GL/glew.h>
#include <glm/gtc/type_ptr.hpp>
#include <boost/format.hpp>
#include <boost/assert.hpp>

#include "Dimensions2D.h"
#include "OpenGLShaderProgram.h"
#include "ColorRGBA.h"
#include "OpenGLMesh.h"
#include "OpenGLShaderUtils.h"
#include "OpenGLUniformVariable.h"
#include "OpenGLTexture.h"
#include "Texel.h"
#include "Mesh.h"




void OpenGLWrapper::EnableCapability(GLenum iCapability)
{
	glEnable(iCapability);
}

void OpenGLWrapper::SetViewportDimensions(const Dimensions2D& iDimensions)
{
	glViewport(0, 0, iDimensions.Width, iDimensions.Height);
}

void OpenGLWrapper::ClearViewport(const ColorRGBA& iWithColor)
{
	glClearColor(iWithColor.Red, iWithColor.Green, iWithColor.Blue, iWithColor.Alpha);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

OpenGLMesh OpenGLWrapper::CreateMesh(const Mesh& iFromMesh)
{
	static const unsigned int sNumberOfCoordinatesPerVertex = 3;
	static const unsigned int sNumberOfCoordinatesPerTexture = 2;
	static const unsigned int sNumberOfCoordinatesPerNormal = 3;

	const std::vector<unsigned int>& indices = iFromMesh.GetIndices();
	const std::vector<glm::vec3>& vertices = iFromMesh.GetVertices();
	const std::vector<glm::vec3>& normals = iFromMesh.GetNormals();
	const std::vector<Texel>& textureMapping = iFromMesh.GetTextureMapping();


	if (!normals.empty() && normals.size() != vertices.size())
	{
		throw std::invalid_argument((boost::format("The number of Normals should match exactly the amount of Vertices in the mesh. Number of Normals = %1% and Number of Vertices = %2%") % normals.size() % (vertices.size())).str());
	}

	if (!textureMapping.empty() && textureMapping.size() != vertices.size())
	{
		throw std::invalid_argument((boost::format("The number of Texels in the texture mapping should match exactly the amount of Vertices in the mesh. Number of Texels = %1% and Number of Vertices = %2%") % textureMapping.size() % (vertices.size())).str());
	}

	std::vector<GLfloat> verticesWithTexturesAndNormals = CombineAllVerticesData(vertices, textureMapping, normals);

	GLuint vao = 0;
	GLuint ibo = 0;
	GLuint vbo = 0;

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	{
		glGenBuffers(1, &ibo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
		{
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

			glGenBuffers(1, &vbo);
			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			{
				glBufferData(GL_ARRAY_BUFFER, verticesWithTexturesAndNormals.size() * sizeof(GLfloat), verticesWithTexturesAndNormals.data(), GL_STATIC_DRAW);

				GLsizei offsetBetweenTwoVertices = sizeof(GLfloat) * (sNumberOfCoordinatesPerVertex + sNumberOfCoordinatesPerTexture + sNumberOfCoordinatesPerNormal);

				// Attribute 0 of vertex shader: vertices of the mesh
				glVertexAttribPointer(0, sNumberOfCoordinatesPerVertex, GL_FLOAT, GL_FALSE, offsetBetweenTwoVertices, 0);
				glEnableVertexAttribArray(0);

				// Attribute 1 of vertex shader: texture mapping of the mesh
				GLsizei offsetBetweenTwoTextureTexels = offsetBetweenTwoVertices;
				void* whereTexelsStartInTheArray = (void*)(sizeof(GLfloat) * sNumberOfCoordinatesPerVertex);
				glVertexAttribPointer(1, sNumberOfCoordinatesPerTexture, GL_FLOAT, GL_FALSE, offsetBetweenTwoTextureTexels, whereTexelsStartInTheArray);
				glEnableVertexAttribArray(1);

				// Attribute 2 of vertex shader: normals of the mesh
				GLsizei offsetBetweenTwoNormals = offsetBetweenTwoVertices;
				void* whereNormalsStartInTheArray = (void*)(sizeof(GLfloat) * (sNumberOfCoordinatesPerVertex + sNumberOfCoordinatesPerTexture));
				glVertexAttribPointer(2, sNumberOfCoordinatesPerNormal, GL_FLOAT, GL_FALSE, offsetBetweenTwoNormals, whereNormalsStartInTheArray);
				glEnableVertexAttribArray(2);
			}
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	}
	glBindVertexArray(0);

	return OpenGLMesh(indices, vertices, normals, vao, ibo, vbo);
}

std::vector<GLfloat> OpenGLWrapper::CombineAllVerticesData(const std::vector<glm::vec3>& iVertices, const std::vector<Texel>& iTextureMapping, const std::vector<glm::vec3>& iNormals)
{
	std::vector<GLfloat> result;

	for (size_t i = 0; i < iVertices.size(); ++i)
	{
		result.push_back(iVertices[i].x);
		result.push_back(iVertices[i].y);
		result.push_back(iVertices[i].z);

		if (iTextureMapping.empty() || iTextureMapping.size() - 1 < i)
		{
			// No texture mapping defined, put default values
			result.push_back(0.f);
			result.push_back(0.f);
		}
		else
		{
			result.push_back(iTextureMapping[i].U);
			result.push_back(iTextureMapping[i].V);
		}

		if (iNormals.empty() || iNormals.size() - 1 < i)
		{
			result.push_back(1.f);
			result.push_back(0.f);
			result.push_back(0.f);
		}
		else
		{
			result.push_back(iNormals[i].x);
			result.push_back(iNormals[i].y);
			result.push_back(iNormals[i].z);
		}
	}

	return result;
}

void OpenGLWrapper::DrawMesh(const OpenGLMesh& iToDraw)
{
	if (iToDraw.GetTexture())
	{
		BindTextureToSampler(GL_TEXTURE0, *iToDraw.GetTexture());
	}

	glBindVertexArray(iToDraw.GetVAO());
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iToDraw.GetIBO());
	glDrawElements(GL_TRIANGLES, iToDraw.GetNumberOfVertices(), GL_UNSIGNED_INT, nullptr);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(ClearVertexArray());
}

OpenGLShader OpenGLWrapper::CreateShader(const Shader& iFromShader) const
{
	GLenum shaderOpenGLType = OpenGLShaderUtils::ToGLenum(iFromShader.GetType());

	GLuint createdShaderId = glCreateShader(shaderOpenGLType);
	if (!createdShaderId)
	{
		throw std::runtime_error((boost::format("glCreateShader call failed to create a %1% shader.") % OpenGLShaderUtils::GLShaderTypeToString(shaderOpenGLType)).str());
	}

	int sourceCodeLength = iFromShader.GetSourceCode().length();
	GLchar* sourceCodeInChar = new GLchar[sourceCodeLength + 1];
	iFromShader.GetSourceCode().copy(sourceCodeInChar, sourceCodeLength + 1);

	const GLchar* sourceCodesInList[1];
	sourceCodesInList[0] = sourceCodeInChar;

	GLint lenghtOfSourceCodesInList[1];
	lenghtOfSourceCodesInList[0] = iFromShader.GetSourceCode().length();

	glShaderSource(createdShaderId, 1, sourceCodesInList, lenghtOfSourceCodesInList);

	delete[] sourceCodeInChar;

	OpenGLShader created;
	created.SetID(createdShaderId);
	created.SetType(shaderOpenGLType);
	created.SetSourceCode(iFromShader.GetSourceCode());
	created.SetUniformVariables(OpenGLShaderUtils::ConvertToOpenGL(iFromShader.GetVariables()));

	return created;
}

void OpenGLWrapper::CompileShader(const OpenGLShader& iToCompile) const
{
	GLuint shaderId = iToCompile.GetID();
	glCompileShader(shaderId);

	GLint compilationResult;
	glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compilationResult);
	if (!compilationResult)
	{
		const GLuint maxSizeOfLog = 1024;
		GLchar compilationError[maxSizeOfLog] = { 0 };
		glGetShaderInfoLog(shaderId, maxSizeOfLog, nullptr, compilationError);
		throw std::runtime_error((boost::format("Error while compiling %1% shader (id=%2%): %3%") % OpenGLShaderUtils::GLShaderTypeToString(iToCompile.GetType()) % iToCompile.GetID() % compilationError).str());
	}
}

OpenGLShaderProgram OpenGLWrapper::CreateShaderProgram()
{
	GLuint createdProgramID = glCreateProgram();
	if (!createdProgramID)
	{
		throw std::runtime_error("Failed to create a shader program.");
	}

	return OpenGLShaderProgram(createdProgramID);
}

void OpenGLWrapper::UseShaderProgram(const OpenGLShaderProgram& iToUse)
{
	glUseProgram(iToUse.GetID());
}

void OpenGLWrapper::ClearShaderProgram()
{
	glUseProgram(0);
}

void OpenGLWrapper::LinkShaderProgram(const OpenGLShaderProgram& iToLink)
{
	// Now that all shaders are attached to the program, let's link the program.
	GLuint idOfProgramToSetup = iToLink.GetID();
	glLinkProgram(idOfProgramToSetup);
	ValidateProgramIsLinked(idOfProgramToSetup);
}

GLenum OpenGLWrapper::ConvertToOpenGL(Image::Formats iImageFormat) const
{
	switch (iImageFormat)
	{
	case Image::Formats::RGB:
		return GL_RGB;
	case Image::Formats::RGBA:
		return GL_RGBA;
	default:
		BOOST_ASSERT_MSG(false, "Can't convert an unknown image format to an OpenGL image format.");
		return GL_INVALID_ENUM;
		break;
	}
}

void OpenGLWrapper::ValidateProgramIsLinked(GLuint iProgramID)
{
	GLint programLinkStatus = 0;
	glGetProgramiv(iProgramID, GL_LINK_STATUS, &programLinkStatus);
	if (!programLinkStatus)
	{
		GLchar linkError[1024];
		glGetProgramInfoLog(iProgramID, sizeof(linkError), nullptr, linkError);

		throw std::runtime_error((boost::format("Shader program '%1%' failed to link: '%2%'") % iProgramID % linkError).str());
	}
}

void OpenGLWrapper::ValidateShaderProgram(const OpenGLShaderProgram& iToValidate)
{
	GLuint idOfProgram = iToValidate.GetID();
	glValidateProgram(idOfProgram);
	ValidateProgramCanExecute(idOfProgram);
}

void OpenGLWrapper::AttachShaderToProgram(const OpenGLShader& iShader, OpenGLShaderProgram& ioAttachTo)
{
	glAttachShader(ioAttachTo.GetID(), iShader.GetID());
}

void OpenGLWrapper::BindUniformVariablesInProgram(OpenGLShaderProgram& ioProgram)
{
	for (OpenGLUniformVariable* variable : ioProgram.GetUniformVariablesCollection().GetAll())
	{
		variable->SetLocation(glGetUniformLocation(ioProgram.GetID(), variable->GetName().c_str()));
	}
}

void OpenGLWrapper::ValidateProgramCanExecute(GLuint idOfProgramToValidate)
{
	GLint isProgramValid = 0;
	glGetProgramiv(idOfProgramToValidate, GL_VALIDATE_STATUS, &isProgramValid);
	if (!isProgramValid)
	{
		GLchar validationError[1024];
		glGetProgramInfoLog(idOfProgramToValidate, sizeof(validationError), nullptr, validationError);

		throw std::runtime_error((boost::format("Shader program '%1%' failed to validate: '%2%'") % idOfProgramToValidate % validationError).str());
	}
}

void OpenGLWrapper::SetUniformVariable(const OpenGLUniformVariableTyped<GLfloat>& iFloat)
{
	glUniform1f(iFloat.GetLocation(), iFloat.GetValue());
}

void OpenGLWrapper::SetUniformVariable(const OpenGLUniformVariableTyped<glm::mat4>& iMatrix4)
{
	glUniformMatrix4fv(iMatrix4.GetLocation(), 1, GL_FALSE, glm::value_ptr(iMatrix4.GetValue()));
}

void OpenGLWrapper::SetUniformVariable(const OpenGLUniformVariableTyped<glm::vec3>& iVector3)
{
	glm::vec3 newValue = iVector3.GetValue();
	glUniform3f(iVector3.GetLocation(), newValue.x, newValue.y, newValue.z);
}

OpenGLTexture OpenGLWrapper::CreateTexture(const Image* const iFromImage)
{
	GLuint createdTextureId;
	glGenTextures(1, &createdTextureId);

	OpenGLTexture createdTexture(createdTextureId);

	glBindTexture(GL_TEXTURE_2D, createdTextureId);

	// WRAP_S refers to the wrapping on the horizontal axis
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	// WRAP_T refers to the wrapping on the vertical axis
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, ConvertToOpenGL(iFromImage->Format), iFromImage->Width, iFromImage->Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, iFromImage->RawData.get());
	glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, 0);

	return createdTexture;
}

void OpenGLWrapper::BindTextureToSampler(GLenum iShaderTextureSampler, const OpenGLTexture& iTexture)
{
	glActiveTexture(iShaderTextureSampler);
	glBindTexture(GL_TEXTURE_2D, iTexture.GetID());
}
