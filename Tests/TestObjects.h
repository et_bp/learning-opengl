#pragma once

#include <vector>
#include <glm/vec3.hpp>
#include "Texel.h"
#include "Mesh.h"

/**
 * Contains objects that can be used as test data for all tests
 */
namespace TestObjects
{
	inline Mesh CreateInvalidMesh()
	{
		// One example of an invalid mesh is one that doesn't have the same amount of vertices and normals

		std::vector<unsigned int> indices =
		{
			0, 1
		};

		std::vector<glm::vec3> vertices =
		{
			{0.f, 0.f, 0.f},
			{1.f, 0.f, 0.f}
		};

		std::vector<glm::vec3> normals = 
		{
			{0.f, 0.f, 0.f}
		};

		return Mesh(indices, vertices, normals, {});
	}

	inline Mesh CreateTriangleMesh(float iScale = 1.f)
	{
		std::vector<unsigned int> indices =
		{
			0, 1, 2
		};

		std::vector<glm::vec3> vertices =
		{
			{0.f, 0.f, 0.f},
			{1.f, 1.f, 0.f},
			{0.f, 1.f, 0.f}
		};
		for (glm::vec3& vertex : vertices)
		{
			vertex *= iScale;
		}

		std::vector<glm::vec3> normals =
		{
			{0.f, 0.f, 1.f},
			{0.f, 0.f, 1.f},
			{0.f, 0.f, 1.f}
		};
		std::vector<Texel> textureMapping =
		{
			{0.f, 0.f},
			{1.f, 1.f},
			{0.f, 1.f}
		};

		return Mesh(indices, vertices, normals, textureMapping);
	}

	inline std::unique_ptr<Mesh> CreateTriangleMeshByAllocation(float iScale = 1.f)
	{
		return std::make_unique<Mesh>(CreateTriangleMesh(iScale));
	}

	inline Material CreateMaterial()
	{
		return Material(
			std::make_shared<Image>(std::make_unique<unsigned char>('0'), 128, 128, Image::Formats::RGBA),
			0.00001f,
			128.f
			);
	}

	inline Mesh CreateTriangleMeshWithMaterial(float iScale = 1.f)
	{
		Mesh created = CreateTriangleMesh();
		created.SetMaterial(CreateMaterial());
		return created;
	}

	inline std::unique_ptr<Mesh> CreateTexturedTriangleMeshByAllocation(float iScale = 1.f)
	{
		return std::make_unique<Mesh>(CreateTriangleMeshWithMaterial(iScale));
	}
}