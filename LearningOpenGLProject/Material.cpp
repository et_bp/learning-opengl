#include "Material.h"


Material::Material(std::shared_ptr<Image> iTexture, float iSpecularIntensity, float iSpecularPrecision)
	: mTexture(iTexture)
	, mSpecularIntensity(iSpecularIntensity)
	, mSpecularPrecision(iSpecularPrecision)
{

}

bool Material::operator==(const Material& iOther) const
{
	return
		mSpecularIntensity == iOther.mSpecularIntensity &&
		mSpecularPrecision == iOther.mSpecularPrecision &&
		mTexture == iOther.mTexture;
}
