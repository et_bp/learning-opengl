#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>
#include <stdexcept>

#include "PhongWorldRenderer.h"
#include "MockedRenderer3D.h"
#include "TestObjects.h"
#include "MeshUniqueIdInRenderer.h"
#include "ColorRGBA.h"
#include "ShaderTestObjects.h"
#include "AmbientLight.h"
#include "DirectionalLight.h"

using ::testing::InSequence;
using ::testing::Return;
using ::testing::Throw;
using ::testing::_;
using ::testing::IsNull;

/**
 * Test fixture where the PhongWorldRenderer has not been initialized yet
 */
class PhongWorldRendererUninitializedTest : public ::testing::Test
{
protected:

	void SetUp() override 
	{
		Renderer3DMock = std::make_unique<MockedRenderer3D>();
		UnderTest = std::make_unique<PhongWorldRenderer>(Renderer3DMock.get());
	}

	std::unique_ptr<PhongWorldRenderer> UnderTest;
	std::unique_ptr<MockedRenderer3D> Renderer3DMock;
};

class PhongWorldRendererTest : public ::testing::Test
{
protected:

	void SetUp() override
	{
		Renderer3DMock = std::make_unique<MockedRenderer3D>();

		UnderTest = std::make_unique<PhongWorldRenderer>(Renderer3DMock.get());

		InitializeAndTestShaders();
	}

	std::unique_ptr<PhongWorldRenderer> UnderTest;
	std::unique_ptr<MockedRenderer3D> Renderer3DMock;

	PhongLightingUniformVariables PhongUniformVariables = ShaderTestObjects::CreatePhongUniformVariables();
	CameraUniformVariables CameraUniformVariables = ShaderTestObjects::CreateCameraUniformVariables();

private:
	 
	void InitializeAndTestShaders()
	{
		Shader vertexShader(
			Shader::Type::Vertex,
			ShaderTestObjects::CreateVertexShaderSourceCode(),
			{ {PhongUniformVariables.GetAllVariablesForShader(Shader::Type::Vertex), CameraUniformVariables.GetAllVariablesForShader(Shader::Type::Vertex)} }
		);

		Shader fragmentShader(
			Shader::Type::Fragment,
			ShaderTestObjects::CreateFragmentShaderSourceCode(),
			{ {PhongUniformVariables.GetAllVariablesForShader(Shader::Type::Fragment),  CameraUniformVariables.GetAllVariablesForShader(Shader::Type::Fragment)} }
		);

		EXPECT_CALL(*Renderer3DMock, SetShader(vertexShader));
		EXPECT_CALL(*Renderer3DMock, SetShader(fragmentShader));
		EXPECT_CALL(*Renderer3DMock, AllShadersAreSet());

		UnderTest->SetShaders(
			vertexShader.GetSourceCode(),
			fragmentShader.GetSourceCode(),
			PhongUniformVariables,
			CameraUniformVariables);
	}
};

TEST_F(PhongWorldRendererTest, MeshInWorldIsRendered)
{
	std::unique_ptr<Mesh> meshToRender = TestObjects::CreateTriangleMeshByAllocation();
	MeshUniqueIdInRenderer meshIdInRenderer(0);

	{
		InSequence nextCallsShouldBeInOrder;

		EXPECT_CALL(*Renderer3DMock, CreateMesh(*meshToRender.get())).WillOnce(Return(meshIdInRenderer));
		EXPECT_CALL(*Renderer3DMock, DrawMesh(meshIdInRenderer));
	}

	UnderTest->AddMeshToWorld(std::move(meshToRender));
	UnderTest->Render();
}

TEST_F(PhongWorldRendererTest, MeshesInWorldAreRendered)
{
	std::vector<std::unique_ptr<Mesh>> meshesToTransferInWorld;
	std::vector<MeshUniqueIdInRenderer> meshesId;

	size_t numberOfMeshesToCreate = 10;
	for (size_t i = 0; i < numberOfMeshesToCreate; ++i)
	{
		float makeThisTriangleUnique = (float)(i + 1);
		meshesToTransferInWorld.push_back(TestObjects::CreateTriangleMeshByAllocation(makeThisTriangleUnique));
		meshesId.push_back(i);
	}

	for (size_t i = 0; i < meshesToTransferInWorld.size(); ++i)
	{
		InSequence nextCallsShouldBeInOrder;

		const std::unique_ptr<Mesh>& meshToRender = meshesToTransferInWorld[i];

		EXPECT_CALL(*Renderer3DMock, CreateMesh(*meshToRender.get())).WillOnce(Return(meshesId[i]));
		EXPECT_CALL(*Renderer3DMock, DrawMesh(meshesId[i]));
	}

	UnderTest->AddMeshesToWorld(std::move(meshesToTransferInWorld));

	UnderTest->Render();

	// All meshes should have been transfered to the world, so nothing is left in the original array 
	ASSERT_EQ(meshesToTransferInWorld.size(), 0);
}

TEST_F(PhongWorldRendererTest, InvalidMeshDoesntGetAddedToWorld)
{
	std::vector<std::unique_ptr<Mesh>> invalidMeshes;
	invalidMeshes.push_back(std::make_unique<Mesh>(TestObjects::CreateInvalidMesh()));

	EXPECT_CALL(*Renderer3DMock, CreateMesh(*invalidMeshes[0].get())).WillOnce(Throw(std::invalid_argument("Mesh failed to be created.")));

	UnderTest->AddMeshesToWorld(std::move(invalidMeshes));

	// Validate the mesh is has not been transfered to the world and is still in the initial array
	ASSERT_TRUE(invalidMeshes.size() == 1);
};

TEST_F(PhongWorldRendererTest, RenderingAMeshWithMaterialSetsTheRightUniformVariables)
{
	auto meshWithMaterial = std::make_unique<Mesh>(TestObjects::CreateTriangleMeshWithMaterial());

	float materialSpecularIntensity = meshWithMaterial->GetMaterial().GetSpecularIntensity();
	float materialSpecularPrecision = meshWithMaterial->GetMaterial().GetSpecularPrecision();

	ON_CALL(*Renderer3DMock, CreateMesh(*meshWithMaterial.get())).WillByDefault(Return(MeshUniqueIdInRenderer(1)));

	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(PhongUniformVariables.GetMaterialSpecularIntensityVariableName(), materialSpecularIntensity));
	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(PhongUniformVariables.GetMaterialSpecularPrecisionVariableName(), materialSpecularPrecision));

	UnderTest->AddMeshToWorld(std::move(meshWithMaterial));
	UnderTest->Render();
}

TEST_F(PhongWorldRendererTest, BackgroundColorIsSetWhenRendering)
{
	EXPECT_CALL(*Renderer3DMock, SetBackgroundColor(_));
	UnderTest->Render();
}

TEST_F(PhongWorldRendererTest, SetAmbientLightUpdatesTheUniformVariablesForThatLight)
{
	AmbientLight newAmbientLight(1.f, ColorRGB(1.f, 0.f, 0.f));

	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(PhongUniformVariables.GetAmbientLightIntensityVariableName(), newAmbientLight.GetIntensity()));
	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(PhongUniformVariables.GetAmbientLightColorVariableName(), glm::vec3(newAmbientLight.GetColor())));

	UnderTest->SetAmbientLight(newAmbientLight);
}

TEST_F(PhongWorldRendererTest, SetDirectionalLightUpdatesTheUniformVariablesForThatLight)
{
	DirectionalLight newDirectionalLight(1.f, ColorRGB(1.f, 0.f, 0.f), glm::vec3(0.f, -1.f, 0.f));

	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(PhongUniformVariables.GetDirectionalLightIntensityVariableName(), newDirectionalLight.GetIntensity()));
	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(PhongUniformVariables.GetDirectionalLightColorVariableName(), glm::vec3(newDirectionalLight.GetColor())));
	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(PhongUniformVariables.GetDirectionalLightDirectionVariableName(), newDirectionalLight.GetDirection()));

	UnderTest->SetDirectionalLight(newDirectionalLight);
}

TEST_F(PhongWorldRendererTest, UpdateModelMatrixUpdatesUniformVariables)
{
	glm::mat4x4 newModelMatrix; newModelMatrix * 999.f;

	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(CameraUniformVariables.GetModelVariableName(), newModelMatrix));

	UnderTest->SetModelmatrix(newModelMatrix);
}

TEST_F(PhongWorldRendererUninitializedTest, UpdateModelMatrixFailsWhenNoShadersAreSet)
{
	glm::mat4x4 newModelMatrix; newModelMatrix * 999.f;
	ASSERT_THROW(UnderTest->SetModelmatrix(newModelMatrix), NoShaderSetError);
}

TEST_F(PhongWorldRendererTest, UpdatePlayerCameraUpdatesUniformVariables)
{
	glm::vec3 newCameraPosition(999.f, 999.f, 999.f);
	glm::mat4x4 newViewMatrix; newViewMatrix * 999.f;

	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(CameraUniformVariables.GetCameraPositionVariableName(), newCameraPosition));
	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(CameraUniformVariables.GetViewVariableName(), newViewMatrix));

	UnderTest->UpdatePlayerCamera(newCameraPosition, newViewMatrix);
}

TEST_F(PhongWorldRendererUninitializedTest, UpdatePlayerCameraFailsWhenNoShadersAreSet)
{
	glm::vec3 newCameraPosition(999.f, 999.f, 999.f);
	glm::mat4x4 newViewMatrix; newViewMatrix * 999.f;
	ASSERT_THROW(UnderTest->UpdatePlayerCamera(newCameraPosition, newViewMatrix), NoShaderSetError);
}

TEST_F(PhongWorldRendererTest, SetProjectionUpdatesUniformVariable)
{
	glm::mat4x4 newProjectionValue; newProjectionValue *= 1000.f;

	EXPECT_CALL(*Renderer3DMock, SetValueInUniformVariable(CameraUniformVariables.GetProjectionVariableName(), newProjectionValue));
	UnderTest->SetProjectionMatrix(newProjectionValue);
}

TEST_F(PhongWorldRendererUninitializedTest, SetProjectionFailsWhenNoShadersAreSet)
{
	glm::mat4x4 newProjectionValue; newProjectionValue *= 1000.f;
	ASSERT_THROW(UnderTest->SetProjectionMatrix(newProjectionValue), NoShaderSetError);
}