#pragma once

#include <functional>
#include "Mesh.h"

/**
 * ID of a mesh registered inside a 3D renderer
 */
struct MeshUniqueIdInRenderer
{
public:
	MeshUniqueIdInRenderer(unsigned int iIdInRenderer)
		: IdInRenderer(iIdInRenderer)
	{}

	bool operator==(const MeshUniqueIdInRenderer& iOther) const
	{
		return
			IdInRenderer == iOther.IdInRenderer;
	}

	bool operator<(const MeshUniqueIdInRenderer& iOther) const
	{
		return IdInRenderer < iOther.IdInRenderer;
	}

	friend std::ostream& operator<<(std::ostream& ioStream, const MeshUniqueIdInRenderer& iToStream);

	unsigned int IdInRenderer = UINT_MAX;
};

struct MeshUniqueIdInRendererHash
{
	std::size_t operator()(const MeshUniqueIdInRenderer& iToHash) const
	{
		return std::hash<unsigned int>()(iToHash.IdInRenderer);
	}
};

