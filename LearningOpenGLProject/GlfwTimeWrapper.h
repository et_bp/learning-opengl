#pragma once
#include "IRealTimeTicker.h"
#include <vector>

/**
 * Wraps the concept of time in Glfw.
 * The concept of time is usually used to determine a delta between two updates
 */
class GlfwTimeWrapper : public IRealTimeTicker
{
public:
	// IRealTimeTicker overrides begin

	void WillTick(IRunsInRealTime* iToTick) override;
	void TickAll() override;

	// IRealTimeTicker overrides end

private:

	float GetDeltaTime();

	std::vector<IRunsInRealTime*> mObjectsToTick;

	float mLastTickTime = 0.f;
};

