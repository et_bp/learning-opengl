#pragma once

class IRunsInRealTime;

/**
 * Ticks all registered IRunsInRealTime classes
 */
class IRealTimeTicker
{
public:
	/** Register a IRunsInRealTime object to be ticked */
	virtual void WillTick(IRunsInRealTime* iToTick) = 0;
	/** Tick all registered objects */
	virtual void TickAll() = 0;
};

