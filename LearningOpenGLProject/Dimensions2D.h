#pragma once

struct Dimensions2D
{
	Dimensions2D() = default;
    Dimensions2D(int iWidth, int iHeight) : Width(iWidth), Height(iHeight) {}
	float GetAspectRatio() const { return (float)Width / (float)Height; }

	bool operator==(const Dimensions2D& iOther) const
	{
		return Width == iOther.Width && Height == iOther.Height;
	}

    int Width;
    int Height;
};