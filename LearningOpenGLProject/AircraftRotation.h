#pragma once

/**
 * Rotation that use the Yaw, Pitch, Roll used in aircraft rotations.
 */
struct AircraftRotation
{
	AircraftRotation(float iYaw, float iPitch, float iRoll)
		: Yaw(iYaw)
		, Pitch(iPitch)
		, Roll(iRoll)
	{}

	float Yaw = 0.f;
	float Pitch = 0.f;
	float Roll = 0.f;
};

